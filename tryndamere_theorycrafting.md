Everything is ranked based on "RealDmg", which is the amount of damage dealt before dying. This metric is more complete than pure DPS.
The incoming damage distribution is balanced (coherent with an opponent tank?) : 25 true dmg/s, 100 physical dmg/s, 100 magical dmg/s.
In this exploration, we'll assume Tryndamere's ult is on CD.

LVL 9 options. Ingame it will be PD or hydra 99% of the games.
For now, let's assume we don't have Lethal Tempo.
All simulations are done with Tryndamere at 50% HP.

## TL & DR
berserkerBoots -> hydra
Then aim for hullbreaker at lvl 13. Add zeal before or after completing hullbreaker.
Then complete PD or buy mythic, default choice is galeForce. IE here is a trap, lethality too (unless the passives are worth it).
Then last item shojin seems promising!

Full build:
**berserkerBoots -> hydra -> hullbreaker -> phantomDancer -> galeForce -> shojin**

## Early game
```
Comparing inventories for Tryndamere with main rune None, minorRune1 AS, minorRune2 AD
| DPS    | Cost | Build                               | SpellSmg | AutoDmg | Turret DPS | RealDmg | TimeAlive |
| ------ | ---- | ----------------------------------- | -------- | ------- | ---------- | ------- | --------- |
| 247.73 | 3400 | [kraken]                            | 47.72    | 375.85  | 138.30     | 1173.23 | 4.74      |
| 240.74 | 3400 | [phantomDancer, 500gOfAD, 300gOfAD] | 44.24    | 367.39  | 149.04     | 1140.13 | 4.74      |
| 221.02 | 3400 | [IE]                                | 43.54    | 334.35  | 118.48     | 1046.71 | 4.74      |
| 205.01 | 3400 | [navori]                            | 71.06    | 279.47  | 113.38     | 970.90  | 4.74      |
| 193.55 | 3400 | [hydra]                             | 52.16    | 278.77  | 126.14     | 916.61  | 4.74      |
| 191.39 | 3300 | [stridebreaker]                     | 42.92    | 276.22  | 124.99     | 906.40  | 4.74      |
| 188.79 | 3100 | [claw]                              | 43.79    | 250.56  | 123.90     | 894.10  | 4.74      |
7 shown builds (initially had 7 builds). Sorted by RealDmg

 without items **Lvl : 9**, baseAd = 162.1, baseAs = 0.87, percentArmorPen = 0.00, critChance = 0.40, haste = 0.00
***********************************************
```
# Second item
Exploring the Hydra route, at LVL 13:
```
Comparing inventories for Tryndamere with main rune None, minorRune1 AS, minorRune2 AD
| DPS    | Cost | Build                                              | SpellSmg | AutoDmg | Turret DPS | RealDmg | TimeAlive |
| ------ | ---- | -------------------------------------------------- | -------- | ------- | ---------- | ------- | --------- |
| 300.17 | 6800 | [hydra, hullbreaker, 200gOfAS, 200gOfAS, 200gOfAS] | 114.38   | 453.19  | 246.07     | 2844.28 | 9.48      |
| 359.94 | 6800 | [hydra, phantomDancer, 800gOfHP]                   | 137.93   | 542.67  | 220.15     | 2793.86 | 7.76      |
| 378.90 | 6800 | [hydra, kraken]                                    | 133.24   | 583.19  | 216.16     | 2549.74 | 6.73      |
| 347.52 | 6800 | [hydra, IE]                                        | 130.68   | 526.43  | 186.54     | 2338.62 | 6.73      |
| 338.87 | 6800 | [hydra, navori]                                    | 194.69   | 446.06  | 180.96     | 2280.37 | 6.73      |
| 336.61 | 6800 | [hydra, galeForce]                                 | 127.49   | 508.99  | 206.49     | 2265.19 | 6.73      |
| 331.77 | 6750 | [hydra, lordDominik, sword]                        | 120.14   | 418.52  | 193.71     | 2232.62 | 6.73      |
| 304.93 | 6700 | [hydra, stridebreaker]                             | 123.96   | 442.25  | 200.11     | 2051.99 | 6.73      |
8 shown builds (initially had 8 builds). Sorted by RealDmg

 without items **Lvl : 13**, baseAd = 178.9, baseAs = 0.95, percentArmorPen = 0.00, critChance = 0.40, haste = 0.00
***********************************************
```
=> Hull becomes very good, but lacks AS. PD second is also very good, but lacks tankiness.

How valuable are stats when we have hydra, hull and zeal? Since we're tanky now, we'll assume LT ON is more reflective of an actual fight:
![](img/20230501151144.png)
=> Interesting ! Crit seems the most needed stat and lethality also appears!

How about hydra and hull ?
![](img/20230501151401.png)

=> Interesting! What seems to happen here is that the small buy in zeal opens up the non linear dmg ramp up of crit*ECdrReset*AS.

What if we just buy crit instead of zeal?
![](img/20230501152131.png)

=> No, buying cloaks is terrible. I think cloak is a bad item in general since the crit rework, crit becomes good in completed items.

=> Ok so I really like the direction of hydra+hull+zeal. Let's explore that.

# Third item?
First some general exploration (this time with Lethal Tempo maxed out):
```
| DPS    | Cost | Build                                      | SpellSmg | AutoDmg | Turret DPS | RealDmg | TimeAlive |
| ------ | ---- | ------------------------------------------ | -------- | ------- | ---------- | ------- | --------- |
| 649.10 | 9400 | [hydra, hullbreaker, phantomDancer, cloak] | 237.68   | 1054.27 | 476.28     | 7459.03 | 11.49     |
| 642.31 | 9600 | [hydra, hullbreaker, kraken]               | 193.58   | 1084.88 | 483.67     | 7381.11 | 11.49     |
| 622.32 | 9600 | [hydra, hullbreaker, IE]                   | 195.90   | 1042.76 | 443.41     | 7151.33 | 11.49     |
| 587.92 | 9200 | [hydra, hullbreaker, lordDominik]          | 177.33   | 818.17  | 454.44     | 6756.00 | 11.49     |
| 534.22 | 8350 | [hydra, hullbreaker, zeal, dirk]           | 168.41   | 845.04  | 446.83     | 6138.93 | 11.49     |
| 531.87 | 9300 | [hydra, hullbreaker, claw]                 | 171.80   | 795.85  | 479.67     | 6111.99 | 11.49     |
| 473.07 | 7250 | [hydra, hullbreaker, zeal]                 | 157.24   | 784.36  | 391.98     | 5436.25 | 11.49     |
| 457.17 | 7300 | [hydra, hullbreaker, dirk]                 | 144.13   | 723.15  | 415.47     | 5253.51 | 11.49     |
```
Here PD+cloak beats kraken and is 200g cheaper! I think it's because AD has became so underwhelming at this stage compared to other stats.

Also zeal+dirk beats a full lordDominik and is 850g cheaper!

=> Since AD is so bad (because we have so much of it), I wonder how strong is an item like wits end ? -> To be explored.


Let's explore around hydra+hull+zeal:
```
Comparing inventories for Tryndamere with main rune Tempo, minorRune1 AS, minorRune2 AD
| DPS    | Cost  | Build                                          | SpellSmg | AutoDmg | Turret DPS | RealDmg | TimeAlive |
| ------ | ----- | ---------------------------------------------- | -------- | ------- | ---------- | ------- | --------- |
| 749.11 | 10650 | [hydra, hullbreaker, zeal, kraken]             | 258.73   | 1271.26 | 529.68     | 9430.16 | 12.59     |
| 544.48 | 10050 | [hydra, hullbreaker, zeal, hullbreaker]        | 182.03   | 930.02  | 464.77     | 9284.00 | 17.05     |
| 736.41 | 10650 | [hydra, hullbreaker, zeal, IE]                 | 238.86   | 1265.19 | 489.36     | 9270.29 | 12.59     |
| 721.04 | 10700 | [hydra, hullbreaker, phantomDancer, cloak, BF] | 266.39   | 1206.29 | 544.96     | 9076.89 | 12.59     |
| 692.11 | 10650 | [hydra, hullbreaker, zeal, navori]             | 357.58   | 1056.00 | 477.06     | 8712.61 | 12.59     |
| 688.31 | 10250 | [hydra, hullbreaker, zeal, lordDominik]        | 216.22   | 974.35  | 502.21     | 8664.81 | 12.59     |
| 679.77 | 10650 | [hydra, hullbreaker, zeal, galeForce]          | 251.14   | 1137.23 | 513.76     | 8557.30 | 12.59     |
| 653.10 | 10350 | [hydra, hullbreaker, zeal, claw]               | 233.20   | 954.62  | 547.89     | 8221.53 | 12.59     |
| 582.09 | 10150 | [hydra, hullbreaker, zeal, 40Lethality]        | 159.39   | 807.00  | 520.26     | 7327.67 | 12.59     |
| 473.16 | 7250  | [hydra, hullbreaker, zeal]                     | 159.39   | 807.00  | 403.29     | 5956.36 | 12.59     |
```
* **What's up with lethality?** I created a fake item that gives 40 lethality, and fully expected it to top the charts. It didn't. I recalculated by hand to check for a bug, but the calculation is correct. It's not that lethality is bad (+24% dmg increase with 2800 gold is good), it's that the other options are much better.
* **What's up with double hullbreaker?** Just tested it to see if the tank route seems viable. Without surprise, the result is very good, but hullbreaker is among the most gold efficient items in the game and gate keeped by the "no allied" passive. And, this is still without accounting for R's +5 sec of survival time. Gut feeling: tank from here is not the best, but hullbreaker second/third item is incredible.
* **Claw has the best turret damage**. But the loss in RealDmg compared to traditionnal options seems too big.
* **GaleForce is a good option** This item is my goto mythic. In this simulation it falls 900 dmg behind Kraken, but all of Kraken's value is visible here. Gale has MS, a dash, and execute damage, that are not visible. Still a solid option.
* **phantomDancer is a good option**. Tops the turret damage, doens't lose much RealDmg, and the MS is very valuable, although not quantifiable here.
* **IE is a mistake**. All it offers is dmg, and at the same cost Kraken deals more RealDmg and more turret dmg.
* **Navori is disapointing**. Coherent with the E resets tests (low value). However it has hidden value in Q and W resets.
* **lordDominik is ... bad!**. In this simulation, against ~100 armor, this item is not worth. Tried against 200 armor, and it was still beaten by Kraken and by IE. At 300 armor, it becomes the best buy with a small margin. I'll stay away from the item unless extreme cases.

## Default 4 items
=> The default 4 item core will probably be:
```
| DPS    | Cost  | Build                                          | SpellSmg | AutoDmg | Turret DPS | RealDmg  | TimeAlive |
| ------ | ----- | ---------------------------------------------- | -------- | ------- | ---------- | -------- | --------- |
| 862.78 | 12200 | [hydra, hullbreaker, phantomDancer, galeForce] | 298.14   | 1386.38 | 611.64     | 10861.19 | 12.59     |
```
**Note** Fixed a bug where the opponent in inventory comparisons (the text charts) had 9 extra armor compared to the opponent in statsWorthitude (the radar graphs). Numbers are now coherent.
![](img/20230501170943.png)

## Fifth item?
Had to code a few new items for this one:
```
|     DPS |   Cost | Build                                                       |   SpellSmg |   AutoDmg |   Turret DPS |   RealDmg |   TimeAlive |
| 1269.33 |  15600 | [hydra, hullbreaker, phantomDancer, galeForce, IE]          |     346.45 |   2131.82 |       716.58 |  15979.02 |       12.59 |
| 1057.45 |  45600 | [hydra, hullbreaker, phantomDancer, galeForce, shojin]      |     457.34 |   1607.25 |       709.08 |  14758.34 |       13.96 |
|  991.56 |  15500 | [hydra, hullbreaker, phantomDancer, galeForce, titanic]     |     340.59 |   1595.36 |       703.84 |  14743.07 |       14.87 |
| 1169.13 |  15600 | [hydra, hullbreaker, phantomDancer, galeForce, navori]      |     543.28 |   1739.35 |       701.59 |  14717.60 |       12.59 |
| 1168.00 |  15200 | [hydra, hullbreaker, phantomDancer, galeForce, lordDominik] |     318.84 |   1627.85 |       749.16 |  14703.38 |       12.59 |
| 1121.07 |  15400 | [hydra, hullbreaker, phantomDancer, galeForce, serylda]     |     329.20 |   1539.29 |       774.81 |  14112.69 |       12.59 |
| 1119.23 |  15200 | [hydra, hullbreaker, phantomDancer, galeForce, collector]   |     336.10 |   1720.77 |       744.29 |  14089.43 |       12.59 |
| 1072.43 |  15200 | [hydra, hullbreaker, phantomDancer, galeForce, youmuu]      |     336.10 |   1573.27 |       772.22 |  13500.27 |       12.59 |
```
* **shojin provides ~47 Haste and ~11% MS at 50% HP** . It deals 8% less damage than IE, but it might be a better choice. Also the winrate on this item has been high (although super niche) since release. It has a similar RealDmg than navori, but has extra MS. Navori really is disapointing.
* **All the lethality and armor pen options get outshined** against an opponent with ~104 armor. This is surprising. I double checked and did not find bugs, but more testing is welcome. I believe the difference between what the radar graphs predict and this, is that the lethality cost given by the Wiki is too generous, and that actual lethality items are less efficient stat wise. However they have potent possives whose value are not visible here.
* **Titanic is an option**. If it's common to fight 1 VS several, then this might be a good call. 

## Forgot the boots...
I should have added the boots on all of these simulations, my bad. However I don't think they shift the discussion too much.
The final "default" build would be: [berserkerBoots, hydra, hullbreaker, phantomDancer, galeForce, shojin]

![](img/20230501180049.png)