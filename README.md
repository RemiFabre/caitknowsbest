[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

## Purpose
A Python library to theorycraft LeagueOfLegends.

## Results and Usage
### How valuable are stats at a given point in the game?
![](img/statsworthitude.png)
If we define: champion, lvl, runes, items, incoming damage composition (true damage, physical damage, magical damage), opponent resistances, etc

Then we can calculate how valuable a given stat is if we want to maximize DPS, turret DPS or real damage done before dying.

In the above example, to maximize the amount of damage dealt before dying, the best stat to buy is... HP!

But, if Tryndamere has his ultimate up, offensive stats outshine defensive stats:
![](img/statsworthitudeultup.png)

Generated with: `statsWorthitudeTest`

# Comparing inventories
Chose a list of inventories and compare them.
Run ```python tryndamere_inventory_tests.py``` for a working example.
Example outputs:
```
| DPS    | Cost | Build                         | SpellSmg | AutoDmg | Turret DPS | RealDmg | TimeAlive |
| ------ | ---- | ----------------------------- | -------- | ------- | ---------- | ------- | --------- |
| 365.18 | 3400 | [kraken]                      | 58.84    | 565.55  | 208.10     | 1729.43 | 4.74      |
| 346.17 | 3400 | [IE]                          | 56.22    | 535.67  | 189.82     | 1639.44 | 4.74      |
| 332.81 | 3300 | [phantomDancer, sword, sword] | 53.66    | 515.38  | 209.08     | 1576.13 | 4.74      |
| 320.70 | 3400 | [galeForce]                   | 57.45    | 490.89  | 199.14     | 1518.80 | 4.74      |
| 311.06 | 3400 | [navori]                      | 84.11    | 447.74  | 181.64     | 1473.13 | 4.74      |
| 289.87 | 3400 | [hydra]                       | 49.01    | 446.62  | 202.09     | 1372.80 | 4.74      |
| 285.11 | 3100 | [claw]                        | 43.71    | 401.42  | 198.50     | 1350.27 | 4.74      |
| 276.01 | 3300 | [stridebreaker]               | 43.54    | 420.29  | 190.18     | 1307.16 | 4.74      |
| 247.37 | 3300 | [goredrinker]                 | 40.55    | 374.31  | 169.37     | 1490.07 | 6.02      |
```

```
Comparing inventories for Tryndamere with main rune Tempo, minorRune1 AS, minorRune2 AD
| DPS    | Cost | Build                                            | SpellSmg | AutoDmg | Turret DPS | RealDmg | TimeAlive |
| ------ | ---- | ------------------------------------------------ | -------- | ------- | ---------- | ------- | --------- |
| 601.00 | 6000 | [phantomDancer, kraken]                          | 166.08   | 970.31  | 328.41     | 4044.35 | 6.73      |
| 582.31 | 6000 | [phantomDancer, IE]                              | 150.12   | 950.94  | 297.54     | 3918.59 | 6.73      |
| 540.42 | 5900 | [phantomDancer, phantomDancer, sword, sword]     | 161.98   | 859.87  | 316.13     | 3636.71 | 6.73      |
| 526.28 | 5950 | [phantomDancer, lordDominik, sword]              | 135.45   | 719.02  | 301.60     | 3541.55 | 6.73      |
| 523.73 | 6000 | [phantomDancer, galeForce]                       | 155.57   | 834.72  | 306.88     | 3524.39 | 6.73      |
| 523.46 | 6000 | [phantomDancer, navori]                          | 210.58   | 779.21  | 286.47     | 3522.61 | 6.73      |
| 478.92 | 6000 | [phantomDancer, hydra]                           | 131.21   | 774.35  | 314.14     | 3222.83 | 6.73      |
| 455.40 | 5900 | [phantomDancer, stridebreaker]                   | 125.28   | 727.24  | 295.03     | 3064.60 | 6.73      |
| 449.31 | 6000 | [phantomDancer, hullbreaker, 300gOfAD, 300gOfAD] | 123.94   | 725.64  | 353.25     | 3534.45 | 7.87      |
```

### Full item exploration
The `idealItemsForLvlVsChampCombi` function is used to brute force item combinations and rank their effeciency to maximize: DPS or turret DPS or real damage done before dying.
A working example: 
```
tryndamereItemOptimization(
        force_lvl=9,
        hobMode=False,
        rune="Tempo",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        isTiamatFirst=False,
        currentPercentHP=0.5,
        ultimate_on=False,
        inputDps=InputDps(25, 100, 100),
    )
```
Gives:
```
Calculating the optimal items for Tryndamere with main rune Tempo, minorRune1 AS, minorRune2 AD
Performing an average dps calculation over an infinite amount of time
There are 40 inventories with 1 items
There are 780 inventories with 2 items
There are 9880 inventories with 3 items
There are 91390 inventories with 4 items
There are 658008 inventories with 5 items
There are 3838380 inventories with 6 items
Dps optimization for champion Tryndamere lvl 9 on 6 available inventory slots with 3415.691 gold
Testing all viable builds
| DPS    | Cost | Build                                                 | SpellSmg | AutoDmg | Turret DPS | RealDmg | TimeAlive |
| ------ | ---- | ----------------------------------------------------- | -------- | ------- | ---------- | ------- | --------- |
| 426.29 | 3400 | [kraken]                                              | 58.84    | 565.55  | 208.10     | 2018.85 | 4.74      |
| 414.85 | 3400 | [cloak, superdagger, zeal, rageknife, cloak]          | 56.73    | 550.90  | 168.97     | 1964.66 | 4.74      |
| 413.43 | 3400 | [phantomDancer, rageknife]                            | 52.51    | 553.04  | 206.32     | 1957.93 | 4.74      |
| 411.71 | 3350 | [cloak, dagger, zeal, rageknife, cloak]               | 56.30    | 546.73  | 167.69     | 1949.81 | 4.74      |
| 411.51 | 3400 | [cloak, dagger, superdagger, zeal, rageknife, dagger] | 52.61    | 550.13  | 184.30     | 1948.86 | 4.74      |
| 406.84 | 3400 | [sword, cloak, zeal, rageknife, cloak]                | 57.36    | 538.53  | 169.90     | 1926.73 | 4.74      |
| 406.71 | 3400 | [sword, cloak, dagger, zeal, rageknife, dagger]       | 53.88    | 541.83  | 186.17     | 1926.13 | 4.74      |
| 406.17 | 3350 | [cloak, superdagger, recurveBow, rageknife, cloak]    | 49.91    | 545.01  | 173.44     | 1923.57 | 4.74      |
| 404.24 | 3400 | [dirk, cloak, dagger, rageknife, cloak]               | 52.55    | 507.20  | 189.19     | 1914.45 | 4.74      |
| 404.11 | 3400 | [IE]                                                  | 56.22    | 535.67  | 189.82     | 1913.80 | 4.74      |
10 shown builds (initially had 10718 builds). Sorted by DPS

 without items **Lvl : 9**, baseAd = 162.1, baseAs = 1.39, percentArmorPen = 0.00, critChance = 0.40, cdr = 0.00
***********************************************


| DPS    | Cost | Build                                                       | SpellSmg | AutoDmg | Turret DPS | RealDmg | TimeAlive |
| ------ | ---- | ----------------------------------------------------------- | -------- | ------- | ---------- | ------- | --------- |
| 389.72 | 3350 | [noonquiver, dirk, dagger, superdagger, dagger]             | 47.06    | 492.59  | 233.90     | 1845.67 | 4.74      |
| 390.65 | 3400 | [sword, noonquiver, dirk, dagger, superdagger]              | 49.38    | 491.55  | 233.40     | 1850.05 | 4.74      |
| 322.24 | 3400 | [dagger, hullbreaker, dagger]                               | 44.31    | 427.68  | 232.22     | 1551.09 | 4.81      |
| 387.79 | 3350 | [sword, noonquiver, dirk, dagger, dagger]                   | 49.38    | 487.59  | 231.53     | 1836.53 | 4.74      |
| 386.36 | 3350 | [BF, dirk, dagger, superdagger, dagger]                     | 49.38    | 485.61  | 230.59     | 1829.76 | 4.74      |
| 386.25 | 3400 | [sword, noonquiver, dirk, dagger, sword]                    | 50.55    | 484.29  | 229.96     | 1829.24 | 4.74      |
| 384.60 | 3400 | [sword, BF, dirk, dagger, superdagger]                      | 50.34    | 482.22  | 228.98     | 1821.44 | 4.74      |
| 387.23 | 3400 | [sword, noonquiver, dagger, superdagger, rageknife, dagger] | 46.69    | 520.49  | 227.40     | 1833.89 | 4.74      |
| 377.49 | 3250 | [BF, noonquiver, dagger, superdagger]                       | 50.54    | 502.37  | 227.32     | 1787.76 | 4.74      |
| 381.31 | 3350 | [sword, BF, dirk, dagger, dagger]                           | 49.91    | 478.09  | 227.02     | 1805.84 | 4.74      |
10 shown builds (initially had 10718 builds). Sorted by Turret DPS

 without items **Lvl : 9**, baseAd = 162.1, baseAs = 1.39, percentArmorPen = 0.00, critChance = 0.40, cdr = 0.00
***********************************************


| DPS    | Cost | Build                                                 | SpellSmg | AutoDmg | Turret DPS | RealDmg | TimeAlive |
| ------ | ---- | ----------------------------------------------------- | -------- | ------- | ---------- | ------- | --------- |
| 426.29 | 3400 | [kraken]                                              | 58.84    | 565.55  | 208.10     | 2018.85 | 4.74      |
| 414.85 | 3400 | [cloak, superdagger, zeal, rageknife, cloak]          | 56.73    | 550.90  | 168.97     | 1964.66 | 4.74      |
| 413.43 | 3400 | [phantomDancer, rageknife]                            | 52.51    | 553.04  | 206.32     | 1957.93 | 4.74      |
| 411.71 | 3350 | [cloak, dagger, zeal, rageknife, cloak]               | 56.30    | 546.73  | 167.69     | 1949.81 | 4.74      |
| 411.51 | 3400 | [cloak, dagger, superdagger, zeal, rageknife, dagger] | 52.61    | 550.13  | 184.30     | 1948.86 | 4.74      |
| 406.84 | 3400 | [sword, cloak, zeal, rageknife, cloak]                | 57.36    | 538.53  | 169.90     | 1926.73 | 4.74      |
| 406.71 | 3400 | [sword, cloak, dagger, zeal, rageknife, dagger]       | 53.88    | 541.83  | 186.17     | 1926.13 | 4.74      |
| 406.17 | 3350 | [cloak, superdagger, recurveBow, rageknife, cloak]    | 49.91    | 545.01  | 173.44     | 1923.57 | 4.74      |
| 404.24 | 3400 | [dirk, cloak, dagger, rageknife, cloak]               | 52.55    | 507.20  | 189.19     | 1914.45 | 4.74      |
| 404.11 | 3400 | [IE]                                                  | 56.22    | 535.67  | 189.82     | 1913.80 | 4.74      |
10 shown builds (initially had 10718 builds). Sorted by RealDmg

 without items **Lvl : 9**, baseAd = 162.1, baseAs = 1.39, percentArmorPen = 0.00, critChance = 0.40, cdr = 0.00
***********************************************
```

### Important stats
`idealBuyForLvlVsChampCombi` calculates which stats are the best at each stage of the game.
But it assumes an empty inventory, so the results are only useful for early game.
This is why we now have "statsWorthitude" that takes into account the inventory.
Working example:
```tryndamereCalculation(bruiserMode=True)```

## TODOs
0) Too many code repetitions around lethality, armor pen, turret armor. Error prone, call a single function for that.

1) Lethal tempo should not activate on turrets.

2) Recreate an useful CLI interface like what we had?
``` 
python3 main.py --lvl 9 --hp 0.01 --dps-mode
```
```
python3 main.py --lvl 18 --hp 1.0 --hob-mode
```

*Notes*
Sometimes when plotting the worthitude of the stats, haste will appear with 0 worth. This is misleading but it's the current desired behaviour.
=> This happens because of the E reset mechanic of Tryndamere. There are some ranges of Haste, where the E CD remains exactly the same, because it's always gonne be reset on the timing of an auto attack. So locally, haste has 0 DPS value. Crits resets are averaged in this calculation, hence the repeatability of this calculation.

The tankiness side project:
https://bitbucket.org/RemiFabre/tanky_lol/src/master/brute_force.py
Is now integrated in here, so no need to install it.