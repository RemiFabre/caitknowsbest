# The idea

Rageknife is a cheap item that comes with a passive that is not balanced on “free crits” champions. People caught up on this on Yas/Yone and it got taken away this patch, a note in the 10.24 patch clearly says that they will implement a larger fix that will most likely impact Tryndamere.

**On Tryndamere, this item is extremely strong in the early game**, and quite weak in the late game because the damage is higher with crits and because of the E resets. **The idea is to get the item as fast as possible (pivot out of crit), abuse its strength in all ins scenarios and turret damage scenarios, and sell it when getting near the point of null value at 233 AD (pivot into crit).** 
For those interested, here is some visualisation of the damage distribution vs crit vs ad:
https://www.reddit.com/r/TryndamereMains/comments/jxfrdk/lets_see_if_someone_can_explain_what_this_3d/


# The setup
Summoners: **Ghost + TP**

Runes: **Future’s market + matchup dependent keystone** or personal preference. However, this strategy works best with AS (so Lethal Tempo is a natural fit but I’m a fan of HoB).

Starter items: **refillable+dagger**

# Early game strategy
**The goal is to aggressively farm the first 2 waves (12 cs, make sure to crush the second wave), recall, buy Rageknife and TP back.** You can actually miss up to 2 cs and be able to get back on time without losing exp.
It is very important to **trade resources as much as possible during those first 2 waves**, use both your refillable pots.
After your TP, you’ll be **lvl 2 with an item that gives 25% AS + 70 damage on hit** when you have full rage. At that point, you’re probably the strongest champion in the game, you have a slow push towards you, you have full ressources and are against a confused opponent that heavily traded with you and lower on resources.

Let the wave come to you and build fury when possible. **All in when appropriate** using ghost if needed. The opponent will either:
- Die or get zoned when you setup your freeze
- Take a back + TP with a buy that will never come close to yours
  
The beauty of this strategy is that **not only is it the highest possible early game DPS, but it also has massive snowball potential through turret damage and plates.**

Another early game benefit is that your CS becomes more efficient. Usually you must account for the probability of a crit messing up your CS, so most players wait until the minion’s health is low enough to guarantee the kill. Now all of your autos deal the same (huge) amount of damage.

# Transition strategy
Early game, AS and crit will maximise your DPS. I believe that PD is a good first item with this strategy, Sanguine blade can be a decent first buy too if you are all-in into split-pushing.
**Berserker’s + rageknife + 2-3 cloaks into PD seems a good staple transition.**

I've seen people experimenting with this but the mistake they make is committing to Rageblade. This guts Tryndamere scaling and takes away the hug power spike that he gets when he can reliably reset his E. Selling Rageknife will only cost 240g.
**Sell Rageknife to complete your second item and build standard from there.**

I say “standard” here but there isn’t a standard build yet. Probably Kraken (for damage) or Galeforce (for mobility) followed by Navori + IE. Yes it overcaps but I still think it’s worth it.
However, I really like early lifesteal on Tryndamere, so I’m considering tweaks to this build. Any suggestions?

**Enjoy**

# Some numbers
This is the result of a DPS calculation made with my program. It simulates a lvl 4 Tryndamere DPSing a lvl 4 Caitlyn for an infinite amount of time (E used on CD, reset mechanic taken into consideration).

Testing all viable builds with 1097.536 gold
22 viable builds (filtered 16 other results)
| DPS   | Cost | Build                   | SpellSmg | AutoDmg | N/A |
| ----- | ---- | ----------------------- | -------- | ------- | --- |
| 85.8  | 0    | [None]                  | 15.3     | 104.6   | 1   |
| 85.8  | 300  | [boots]                 | 15.3     | 104.6   | 1   |
| 86.9  | 900  | [ionanBoots]            | 16.9     | 104.6   | 1   |
| 93.8  | 600  | [cloak]                 | 17.5     | 113.6   | 1   |
| 93.8  | 900  | [boots, cloak]          | 17.5     | 113.6   | 1   |
| 93.9  | 300  | [dagger]                | 15.9     | 115.3   | 1   |
| 93.9  | 600  | [dagger, boots]         | 15.9     | 115.3   | 1   |
| 94.3  | 350  | [sword]                 | 17.1     | 114.7   | 1   |
| 94.3  | 650  | [sword, boots]          | 17.1     | 114.7   | 1   |
| 102.0 | 600  | [dagger, dagger]        | 16.6     | 126.1   | 1   |
| 102.0 | 900  | [dagger, boots, dagger] | 16.6     | 126.1   | 1   |
| 102.8 | 900  | [dagger, cloak]         | 18.4     | 125.3   | 1   |
| 102.8 | 700  | [sword, sword]          | 18.9     | 124.9   | 1   |
| 102.8 | 1000 | [sword, boots, sword]   | 18.9     | 124.9   | 1   |
| 103.1 | 950  | [sword, cloak]          | 19.6     | 124.6   | 1   |
| 103.2 | 650  | [sword, dagger]         | 17.8     | 126.5   | 1   |
| 103.2 | 950  | [sword, dagger, boots]  | 17.8     | 126.5   | 1   |
| 107.1 | 875  | [pickAxe]               | 19.7     | 129.9   | 1   |
| 112.1 | 950  | [sword, dagger, dagger] | 18.5     | 138.3   | 1   |
| 112.5 | 1000 | [sword, dagger, sword]  | 19.6     | 137.7   | 1   |
| 112.9 | 1000 | [recurveBow]            | 16.6     | 141.2   | 1   |
| 124.1 | 800  | [rageknife]             | 9.4      | 164.1   | 1   |

 without items **Lvl : 4**, baseAd = 103.0, baseAs = 0.78, percentArmorPen = 0.00, critChance = 0.40, cdr = 0.00

 # Notes

It is possible to back after the first 6 minions and get Rageknife if you didn't buy refillable but it's better to abuse the refill mechanic and setup the wave bounce.

I tried to do this without using TP but it's not worth it. You'll lose exp from 2 minions (if very lucky) up to 4 minions (common), which is a disaster in the early game. Also TP+ghost scale really will with Tryndamere.

Demolisher synergises really well with this strategy.

I knew this was strong quite early into pre-season but didn't want this strategy to reduce the inevitable buff that eventually came, so I delayed this post. Sorry!