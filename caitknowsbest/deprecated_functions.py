# Collection of old functions that are not used anymore but might be useful later


# This function brute forces every combination of items as long as the money allows it.
# For simplification purposes, you can only buy an item once
def idealItemsForLvlVsChamp(
    champ, sandBag, listOfSpells, fightDuration, lvl, rich=False
):
    if rich:
        totalGold = 30000
    else:
        totalGold = getGoldForLvl(lvl)
    iters = 0
    inventory = []
    idealDmg = 0
    idealSpellDmg = 0
    idealAutoDmg = 0
    idealNbAutos = 0
    idealInventory = []
    idealCost = 0
    baseArmor = sandBag.armor + 9

    listOfItems = ITEMS.values()
    combinations = []

    # The max 5 Qs are a decent approximation for a 4 sec fight. TODO. Update the number of Q with AS and the duration of the fight
    # For printing purposes
    spellsStr = ""
    for s in listOfSpells:
        spellsStr = spellsStr + s.name + " "
    print(
        "\nSimulation of a "
        + "{0:.1f}".format(fightDuration)
        + " seconds fight where "
        + champ.name
        + " hits the following spells : "
        + spellsStr
        + " and as many Qs as possible (limited to 5)"
    )
    # Getting all the combinations using each item only once, from size 1 to 5.
    for size in range(1, 6):
        combinations.extend(itertools.combinations(listOfItems, size))
    print("(" + str(len(combinations)) + " possible item combinations)")

    for qKey, quint in QUINTS.items():
        # We chose our quints
        for mKey, mark in MARKS.items():
            # We chose our marks, let's chose our items
            for inv in combinations:
                iters = iters + 1
                cost = inventoryCost(inv)
                if cost > totalGold:
                    # Too expensive
                    continue

                # We have a viable build, let's measure its performance
                inventory = []
                inventory.append(quint)
                inventory.append(mark)
                inventory.extend(inv)
                champ.inventory = inventory
                champ.lvl = lvl
                champ.update()
                """
                strInventory = ""
                for i in inventory:
                    strInventory = strInventory + " " +  str(i)
                print("Trying inventory :" + strInventory)
                """
                bonusArPen = champ.lethality * (0.6 + 0.4 * sandBag.lvl / 18.0)
                # ArPen can't reduce armor below 0 (armor reduction can though)
                sandBagArmor = max(0, baseArmor - bonusArPen) * (
                    1 - champ.percentArmorPen
                )
                # We use at least 1 auto no matter what
                nbAutos = max(1, fightDuration * champ.as_)
                # Making sure there are not more Q's than auto attacks, with a maximum of 5. This is Draven specific :/ TODO
                tempSpells = []
                for s in listOfSpells:
                    tempSpells.append(s)
                nbAdd = min(5, int(math.floor(nbAutos)))
                for index in range(nbAdd + 1):
                    tempSpells.append(champ.spells[1])

                dmg, spellDmg, autoDmg = champ.getDmgFromActions(tempSpells, nbAutos)
                dmg = dmg * (100.0 / (100 + sandBagArmor))
                spellDmg = spellDmg * (100.0 / (100 + sandBagArmor))
                autoDmg = autoDmg * (100.0 / (100 + sandBagArmor))
                if dmg > idealDmg:
                    idealDmg = dmg
                    idealSpellDmg = spellDmg
                    idealAutoDmg = autoDmg
                    idealNbAutos = nbAutos
                    idealInventory = inventory
                    idealCost = cost

    # This resets the champions stats (that were modified by items), just to print them
    champ.updateStatsForLvl(lvl)
    print(
        "\n **Lvl : "
        + str(lvl)
        + ", gold = "
        + str(totalGold).strip()
        + "**, baseAd = "
        + "{0:.1f}".format(champ.ad)
        + ", baseAs = "
        + "{0:.1f}".format(champ.as_)
        + ", Sandbag's armor = "
        + "{0:.1f}".format(baseArmor)
        + ", iter = "
        + "{0:.1f}".format(iters)
        + ", wastedGold = "
        + str(totalGold - idealCost)
    )
    return [idealDmg, idealSpellDmg, idealAutoDmg, idealNbAutos, idealInventory]


def dravenItemOptimization():
    inventory = []
    draven = Draven(lvl=1, inventory=inventory)
    spells = draven.spells
    P = spells[0]
    Q = spells[1]
    W = spells[2]
    E = spells[3]
    R = spells[4]

    # Qs are added directly in the function
    listOfSpells = [E]
    fightDuration = 4

    # Explores every item combination with the given gold
    rich = False
    for lvl in range(1, 20):
        if lvl == 19:
            lvl = 18
            rich = True
        sandBag = Cait(lvl=lvl, inventory=[])
        sandBag.update()
        if lvl == 6:
            listOfSpells.insert(0, R)
        dmg, spellDmg, autoDmg, nbAutos, inventory = idealItemsForLvlVsChamp(
            draven, sandBag, listOfSpells, fightDuration, lvl, rich=rich
        )
        print(
            "\n**Total dmg = "
            + "{0:.1f}".format(dmg).strip()
            + "**, spellDmg = "
            + "{0:.1f}".format(spellDmg)
            + ", AADmg = "
            + "{0:.1f}".format(autoDmg)
            + ", nbAutos = "
            + "{0:.2f}".format(nbAutos)
        )
        strInventory = ""
        for i in inventory:
            strInventory = strInventory + " " + str(i)
        print("\nWith inventory :**" + str(strInventory).strip() + "**")
        # raw_input("wait")

    return


def dravenCalculation():
    inventory = []
    draven = Draven(lvl=1, inventory=inventory)
    spells = draven.spells
    P = spells[0]
    Q = spells[1]
    W = spells[2]
    E = spells[3]
    R = spells[4]

    listOfSpells = [
        E,
        Q,
        Q,
    ]
    fightDuration = 2

    # Explores dmg, as, crit and lethality vs a champion whose armor scales with lvls
    for lvl in range(1, 19):
        sandBag = Cait(lvl, [])
        sandBag.update()
        if lvl == 6:
            listOfSpells.insert(0, R)
        (
            dmg,
            idealAd,
            idealAs,
            idealCrit,
            idealLethality,
            ieOn,
            spellDmg,
            autoDmg,
            nbAutos,
        ) = idealBuyForLvlVsChamp(
            draven, sandBag, listOfSpells, fightDuration, lvl, 100
        )
        print(
            "Total dmg = ",
            "{0:.1f}".format(dmg),
            " buying ad = ",
            "{0:.1f}".format(idealAd),
            ", as = ",
            "{0:.1f}".format(idealAs),
            ", crit = ",
            "{0:.1f}".format(idealCrit),
            ", lethality = ",
            "{0:.1f}".format(idealLethality),
            ", IE on = ",
            ieOn,
            ", spellDmg = ",
            "{0:.1f}".format(spellDmg),
            ", AADmg = ",
            "{0:.1f}".format(autoDmg),
            ", nbAutos = ",
            "{0:.2f}".format(nbAutos),
        )
    return