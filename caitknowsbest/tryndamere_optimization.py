import datetime
import itertools

import argparse
from argparse import RawTextHelpFormatter

from champ import *
from spell import *
from item import *
from items import *

from utils import (
    getGoldForLvl,
    inventoryCost,
    sort_dict,
    removeInvDuplicates,
    printResults,
    compareInventories,
    InputDps,
)

from optimization import (
    idealBuyForLvlVsChampCombi,
    idealItemsForLvlVsChampCombi,
    realDamageAndTimeAlive,
)
from tankines_brute import tankiness


def tryndamereCalculation(bruiserMode=False, inputDps=InputDps(25, 100, 100)):
    inventory = []
    tryn = Tryndamere(lvl=1, inventory=inventory)
    spells = tryn.spells
    P = spells[0]
    Q = spells[1]
    W = spells[2]
    E = spells[3]
    R = spells[4]
    tryn.rune = "None"
    tryn.hasTranscendence = False
    tryn.hasCosmic = False
    tryn.minorRune1 = "AS"
    tryn.minorRune2 = "AD"
    print(
        "Calculating the optimal stats for Tryndamere with main rune {}, minorRune1 {}, minorRune2 {}, hasTranscendence {}, hasCosmic {}".format(
            tryn.rune,
            tryn.minorRune1,
            tryn.minorRune2,
            tryn.hasTranscendence,
            tryn.hasCosmic,
        )
    )

    # Explores dmg, as, crit and lethality vs a champion whose armor scales with lvls
    header = [
        "lvl",
        "totalGold",
        "goldStep",
        "noItemAd",
        "noItemAs(Hz)",
        "sandBagArmor",
        "totalDps",
        "boughtAd",
        "boughtAs(%)",
        "boughtCrit",
        "boughtLethality",
        "boughtHaste",
        "spellDmg",
        "AADmg",
        "nbCombinations",
    ]
    if bruiserMode:
        header.append("boughtHp")
        header.append("boughtArmor")
        header.append("TimeAlive")
        header.append("DamageDone")
    data = []
    for lvl in range(1, 19):
        sandBag = Cait(lvl=lvl)
        sandBag.update()
        (
            dmg,
            idealAd,
            idealAs,
            idealCrit,
            idealLethality,
            idealHaste,
            spellDmg,
            autoDmg,
            nbAutos,
            totalGold,
            goldStep,
            noItemAd,
            noItemAs,
            sandBagArmor,
            nbCombinations,
            boughtHp,
            boughtArmor,
            TimeAlive,
            DamageDone,
        ) = idealBuyForLvlVsChampCombi(
            tryn,
            sandBag,
            [],
            0,
            lvl,
            stepsPerSearch=10,
            dpsVersion=True,
            bruiserMode=bruiserMode,
            inputDps=inputDps,
        )
        data.append(
            [
                lvl,
                totalGold,
                "{0:.0f}".format(goldStep),
                "{0:.1f}".format(noItemAd),
                "{0:.2f}".format(noItemAs),
                "{0:.1f}".format(sandBagArmor),
                dmg,
                idealAd,
                idealAs * 100,
                idealCrit * 100,
                idealLethality,
                idealHaste,
                spellDmg,
                autoDmg,
                nbCombinations,
            ]
        )
        if bruiserMode:
            data[-1].append(boughtHp)
            data[-1].append(boughtArmor)
            data[-1].append(TimeAlive)
            data[-1].append(DamageDone)
        to_print = "Total dps = {:.1f} buying ad = {:.1f} , as = {:.1f}, crit = {:.1f}%, lethality = {:.1f}, haste = {:.1f}, spellDmg = {:.1f}, AADmg = {:.1f}".format(
            dmg,
            idealAd,
            idealAs * 100,
            idealCrit * 100,
            idealLethality,
            idealHaste,
            spellDmg,
            autoDmg,
        )
        if bruiserMode:
            to_print += f", boughtHp = {boughtHp:.1f}, boughtArmor = {boughtArmor:.1f}, TimeAlive = {TimeAlive:.1f}, DamageDone = {DamageDone:.1f}"
        print(to_print)
    print(tabulate(data, headers=header, tablefmt=""))
    toBeSaved = tabulate(data, headers=header, tablefmt="github")
    path = (
        "results/tryndamere/optimal_stats/"
        + str(tryn.rune)
        + "_"
        + datetime.datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
        + ".md"
    )
    f = open(path, "w+")
    f.write(toBeSaved)
    f.close()

    return


def tryndamereItemOptimization(
    force_lvl=0,
    hobMode=False,
    rune="None",
    minorRune1="AS",
    minorRune2="AD",
    hasTranscendence=True,
    hasCosmic=False,
    isTiamatFirst=True,
    currentPercentHP=0.5,
    ultimate_on=False,
    inputDps=InputDps(25, 100, 100),
):
    inventory = []
    tryn = Tryndamere(lvl=1, inventory=inventory)
    tryn.currentPercentHP = currentPercentHP
    tryn.rune = rune
    tryn.hasTranscendence = hasTranscendence
    tryn.hasCosmic = hasCosmic
    tryn.minorRune1 = minorRune1
    tryn.minorRune2 = minorRune2
    print(
        "Calculating the optimal items for Tryndamere with main rune {}, minorRune1 {}, minorRune2 {}".format(
            tryn.rune, tryn.minorRune1, tryn.minorRune2
        )
    )
    if hobMode:
        print("Using the exact pattern calculator (E+3xAA + E resets if applicable)")
    else:
        print("Performing an average dps calculation over an infinite amount of time")

    listOfItems = ITEMS.values()
    slotsAvailable = 6
    combi = []
    combi.append([[Item("None", 0, 0, [], isUnique=True)]])
    # Getting all the combinations using each item only once, from size 1 to 5.
    for size in range(1, slotsAvailable + 1):
        tempCombi = []
        tempCombi.extend(itertools.combinations(listOfItems, size))
        # tempCombi = removeInvDuplicates(tempCombi)
        print("There are {} inventories with {} items".format(len(tempCombi), size))
        for i in range(len(tempCombi)):
            tempCombi[i] = list(tempCombi[i])
        combi.append(tempCombi)

    # Explores every item combination with the given gold
    rich = False
    for lvl in range(1, 20):
        if force_lvl != 0 and lvl != force_lvl:
            continue
        if lvl == 19:
            lvl = 18
            rich = True
        sandBag = Cait(lvl=lvl, inventory=[])
        sandBag.update()
        results, header = idealItemsForLvlVsChampCombi(
            combi,
            tryn,
            sandBag,
            [],
            0,
            lvl,
            rich=rich,
            dpsVersion=True,
            isTiamatFirst=isTiamatFirst,
            bootsMandatoryLvl=11,
            hobMode=hobMode,
            ultimate_on=ultimate_on,
            inputDps=inputDps,
        )
        # Creating 3 tables, each ranked by : "DPS", "Turret DPS" and "RealDmg"
        printResults(results, tryn, header, sortIndexes=[0, 5, 6], limitResults=10)

    return


def tryndamereCompareInventories(
    lvl,
    listOfInventories,
    dpsVersion=False,
    hobMode=False,
    rune="None",
    minorRune1="AS",
    minorRune2="AD",
    hasTranscendence=False,
    hasCosmic=False,
    currentPercentHP=0.5,
    ultimate_on=False,
    inputDps=InputDps(25, 100, 100),
    sortIndexes=[0, 5, 6],
):
    inventory = []
    tryn = Tryndamere(lvl=lvl, inventory=inventory)
    tryn.currentPercentHP = currentPercentHP
    tryn.rune = rune
    tryn.hasTranscendence = hasTranscendence
    tryn.hasCosmic = hasCosmic
    tryn.minorRune1 = minorRune1
    tryn.minorRune2 = minorRune2
    print(
        "Comparing inventories for Tryndamere with main rune {}, minorRune1 {}, minorRune2 {}".format(
            tryn.rune, tryn.minorRune1, tryn.minorRune2
        )
    )
    results = []
    # sandBag = Cait(lvl=lvl, inventory=[])
    sandBag = Tryndamere(lvl=lvl, inventory=[])
    sandBag.update()
    # baseArmor = 100
    baseArmor = sandBag.armor
    champ = tryn
    fightDuration = 0
    header = [
        "DPS",
        "Cost",
        "Build",
        "SpellSmg",
        "AutoDmg",
        "Turret DPS",
        "RealDmg",
        "TimeAlive",
    ]

    for inv in listOfInventories:
        inventory = []
        inventory.extend(inv)
        champ.inventory = inventory
        champ.lvl = lvl
        champ.update()
        cost = inventoryCost(inv)
        """
        strInventory = ""
        for i in inventory:
            strInventory = strInventory + " " +  str(i)
        print("Trying inventory :" + strInventory)
        """
        bonusArPen = champ.lethality  # champ.lethality * (0.6 + 0.4 * champ.lvl / 18.0)
        # ArPen can't reduce armor below 0 (armor reduction can though)
        sandBagArmor = max(0, baseArmor * champ.percentArmorValid - bonusArPen)
        armorRatio = 100.0 / (100 + sandBagArmor)
        # print(f"base armor {baseArmor}. After reduction armorRatio {armorRatio} for inventory {inventory}")
        turretArmor = 70
        if not (hobMode):
            if not (dpsVersion):
                print("This is deprecated. Choose either hobMode or dpsVersion")
                sys.exit()
            else:
                dmg, spellDmg, autoDmg = champ.getAverageDps(armorRatio)
                turretDps = champ.getAverageDpsTurret(
                    100.0
                    / (100 + max(0, turretArmor * champ.percentArmorValid - bonusArPen))
                )

            realDamage, timeAlive = realDamageAndTimeAlive(
                champ, dmg, inputDps, ultimate_on=ultimate_on
            )
            results.append(
                [
                    dmg,
                    cost,
                    inventory,
                    spellDmg,
                    autoDmg,
                    turretDps,
                    realDamage,
                    timeAlive,
                ]
            )
        else:
            # HoB trading pattern mode
            champ.hailTradingPattern(
                armorRatio=armorRatio, opponentHp=(champ.hpMax + 60), verbose=True
            )

    if not (hobMode):
        # sortIndexes=[0, 5, 6] =>Creating 3 tables, each ranked by : "DPS", "Turret DPS" and "RealDmg"
        printResults(results, tryn, header, sortIndexes=sortIndexes)
    return


if __name__ == "__main__":
    # Calculating which stats are the best at each stage of the game.
    # Interesting, but it assumes an empty inventory, so the results are only useful for early game.
    # This is why we now have "statsWorthitude" that takes into account the inventory.
    tryndamereCalculation(bruiserMode=True)

    # Calculates the best items for a given level
    # tryndamereItemOptimization(
    #     force_lvl=9,
    #     hobMode=False,
    #     rune="Tempo",
    #     minorRune1="AS",
    #     minorRune2="AD",
    #     hasTranscendence=False,
    #     hasCosmic=False,
    #     isTiamatFirst=False,
    #     currentPercentHP=0.5,
    #     ultimate_on=False,
    #     inputDps=InputDps(25, 100, 100),
    # )
