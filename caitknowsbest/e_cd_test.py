from utils import calculate_duration_for_E_reset_navori


if __name__ == "__main__":
    print(calculate_duration_for_E_reset_navori(1.0, 0.0, 0.0))
    print(calculate_duration_for_E_reset_navori(2.0, 0.0, 0.0))
    print(calculate_duration_for_E_reset_navori(2.5, 0.0, 0.0))

    print(calculate_duration_for_E_reset_navori(1.0, 50.0, 1.0))
    print(calculate_duration_for_E_reset_navori(2.0, 50.0, 1.0))
    print(calculate_duration_for_E_reset_navori(2.5, 50.0, 1.0))
