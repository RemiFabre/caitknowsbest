# -*- coding: utf-8 -*-
# Author : Rémi Fabre

# Les batards !
# StatIncrease = g (0.65 + 0.035 * NewLevel)

import math
import sys
from spell import *
from item import *
import scipy.special
from tabulate import tabulate
from item import containsItem
from utils import calculate_duration_for_E_reset_navori


class Champ:
    def __init__(
        self,
        name,
        lvl,
        inventory,
        spells,
        spellsOrder,
        ad0,
        adPlus,
        as0,
        asPlus,
        hp0,
        hpPlus,
        armor0,
        armorPlus,
        mr0,
        mrPlus,
        ms,
        currentPercentHP=0.5,
    ):
        self.name = name
        self.ad = 0
        self.ad0 = ad0
        self.adPlus = adPlus
        self.noItemAd = 0

        self.as_ = 0
        self.as0 = as0
        self.asPlus = asPlus

        self.hpMax = 0
        self.hp = 0
        self.hp0 = hp0
        self.hpPlus = hpPlus
        self.currentPercentHP = currentPercentHP

        self.armor = 0
        self.armor0 = armor0
        self.armorPlus = armorPlus

        self.mr = 0
        self.mr0 = mr0
        self.mrPlus = mrPlus

        self.ms = ms
        self.haste = 0
        self.percentArmorValid = 1
        self.percentArmorPen = 0
        self.lethality = 0

        self.lvl = lvl
        self.inventory = inventory
        self.spells = spells
        self.spellsOrder = spellsOrder
        self.hasIe = False
        self.hasTrinity = False
        self.hasGuinsoo = False
        self.hasRageknife = False
        self.hasKraken = False
        self.hasHullbreaker = False
        self.hasSanguine = False
        self.hasTiamat = False
        self.hasProfane = False
        self.hasNavori = False
        self.hasEssence = False
        self.hasStridebreaker = False
        self.hasGoredrinker = False
        self.hasClaw = False
        self.hasTitanic = False
        self.hasShojin = False

        self.bonusOnHit = 0
        self.critChance = 0
        self.bonusBurst = 0
        self.bonusAs = 0

        self.hasTranscendence = False
        self.hasCosmic = False
        self.minorRune1 = "AD"  # Options are AD, AS, CDR
        self.minorRune2 = "AD"  # Options are AD

        # Calculating current state
        self.updateStatsForLvl(lvl)

    def __repr__(self):
        output = []
        for key in self.__dict__:
            output.append("{key}='{value}'\n".format(key=key, value=self.__dict__[key]))
        return ", ".join(output)

    def updateStatsForLvl(self, lvl):
        # Attention! Double check this. The following line was not present. I think it was a bug that never had an impact because we create a Champion instance instead of leveling up?
        self.lvl = lvl
        # Calculating current state
        # A les batards !
        self.ad = self.ad0 + self.adPlus * (lvl - 1) * (0.685 + 0.0175 * lvl)
        self.noItemAd = self.ad
        # A les chiens galeux !
        self.as_ = (
            self.as0 * (1 + self.asPlus * (lvl - 1) * (0.685 + 0.0175 * lvl))
            + self.bonusAs
        )

        self.hpMax = self.hp0 + self.hpPlus * (lvl - 1) * (0.685 + 0.0175 * lvl)
        self.hp = self.currentPercentHP * self.hpMax
        self.armor = self.armor0 + self.armorPlus * (lvl - 1) * (0.685 + 0.0175 * lvl)
        self.mr = self.mr0 + self.mrPlus * (lvl - 1) * (0.685 + 0.0175 * lvl)

        self.lethality = 0
        self.bonusOnHit = 0
        self.critChance = 0
        self.bonusBurst = 0
        self.haste = 0
        self.percentArmorPen = 0
        self.percentArmorValid = 1.0
        self.hasIe = False
        self.hasTrinity = False
        self.hasGuinsoo = False
        self.hasRageknife = False
        self.hasKraken = False
        self.hasHullbreaker = False
        self.hasSanguine = False
        self.hasTiamat = False
        self.hasProfane = False
        self.hasNavori = False
        self.hasEssence = False
        self.hasStridebreaker = False
        self.hasGoredrinker = False
        self.hasTitanic = False
        self.hasClaw = False
        self.hasShojin = False

        # Upgrading the spell accordingly to spellsOrder :
        qLevel = 0
        wLevel = 0
        eLevel = 0
        rLevel = 0
        for l in range(lvl):
            spellId = self.spellsOrder[l]
            if spellId == 1:
                qLevel = qLevel + 1
            elif spellId == 2:
                wLevel = wLevel + 1
            elif spellId == 3:
                eLevel = eLevel + 1
            elif spellId == 4:
                rLevel = rLevel + 1
        # print "Cait lvl ", self.lvl, ", spells : ", qLevel, ", ", wLevel, ", ", eLevel, ", ", rLevel
        self.spells[1].lvl = qLevel
        self.spells[2].lvl = wLevel
        self.spells[3].lvl = eLevel
        self.spells[4].lvl = rLevel
        # Checking if the spells grant any passive stats
        for s in self.spells:
            if s.lvl > 0:
                self.ad += s.bonusAdPerLvl[s.lvl - 1]
                if self.name == "Tryndamere" and (s.bonusAdPerLvl[s.lvl - 1] > 0):
                    # Not my best integration
                    self.ad += (
                        (0.15 + (0.1) * (s.lvl - 1)) * (1 - self.currentPercentHP) * 100
                    )
        # Checking runes effects
        if self.hasTranscendence and self.lvl >= 5:
            self.haste += 5
        if self.hasTranscendence and self.lvl >= 8:
            self.haste += 5
        if self.minorRune1 == "AD":
            self.ad += 5.6
        elif self.minorRune1 == "AS":
            self.as_ += 0.1 * self.as0
        elif self.minorRune1 == "CDR":
            self.haste += 8
        else:
            print("Error unknown minorRune1")
            sys.exit()
        if self.minorRune2 == "AD":
            self.ad += 5.6
        # Champion specific stuff
        self.specificStatsUpdate(lvl)

    def specificStatsUpdate(self, lvl):
        print("specificStatsUpdate is champion specific, to be overloaded.")

    def updateStatsFromItems(self):
        bonusHP = 0
        for item in self.inventory:
            self.ad = self.ad + item.bonusAd
            self.as_ = self.as_ + item.bonusAs * self.as0
            self.hpMax = self.hpMax + item.bonusHp
            bonusHP += (
                item.bonusHp
            )  # TODO update this for champs that have innate bonus HP
            self.armor = self.armor + item.bonusArmor
            self.mr = self.mr + item.bonusMr
            self.bonusOnHit = self.bonusOnHit + item.bonusOnHit
            self.critChance = self.critChance + item.bonusCritChance
            if self.critChance > 1.0:
                self.critChance = 1.0
            self.bonusBurst = self.bonusBurst + item.bonusBurst
            self.lethality = self.lethality + item.bonusLethality
            self.haste = self.haste + item.bonusHaste
            self.percentArmorValid = self.percentArmorValid * (1 - item.percentArmorPen)
            self.percentArmorPen = 1 - self.percentArmorValid
            # Special items management
            if item.name == "IE":
                self.hasIe = True
            if item.name == "trinityForce":
                self.hasTrinity = True
            if item.name == "chargedGuinsoo":
                self.hasGuinsoo = True
            if item.name == "kraken":
                self.hasKraken = True
                # Accounting for mythic passive
                for _item in self.inventory:
                    if _item.isFinished:
                        self.as_ += (0.1) * self.as0
            if item.name == "goredrinker":
                self.hasGoredrinker = True
                # Accounting for mythic passive
                for _item in self.inventory:
                    if _item.isFinished:
                        self.haste += 3
                        self.hpMax += 50
            if item.name == "claw":
                self.hasClaw = True
                # Accounting for mythic passive
                for _item in self.inventory:
                    if _item.isFinished:
                        self.lethality += 5
            if item.name == "rageknife":
                self.hasRageknife = True
            if item.name == "sanguineBlade":
                self.hasSanguine = True
                self.as_ += (0.16471 + self.lvl * 0.03529) * self.as0
            if item.name == "tiamat" or item.name == "hydra" or item.name == "profane":
                self.hasTiamat = True
            if item.name == "profane":
                self.hasProfane = True
            if item.name == "navori":
                self.hasNavori = True
            if item.name == "essenceReaver":
                self.hasEssence = True
            if item.name == "stridebreaker":
                self.hasStridebreaker = True
            if item.name == "hullbreaker":
                self.hasHullbreaker = True
                bonusResist = 10
                if self.lvl == 12:
                    bonusResist = 20
                elif self.lvl == 13:
                    bonusResist = 35
                else:
                    bonusResist = 35 + (self.lvl - 13) * 8
                self.mr += bonusResist
                self.armor += bonusResist
        # Special effects that need to be accounted for after all items have been used (generally based off of a value that changes with items)
        self.hp = self.currentPercentHP * self.hpMax
        for item in self.inventory:
            if item.name == "titanic":
                self.hasTitanic = True
                # To simplify the on hit (that is one of the few that apply to turrets),
                # we'll assume that the ad converion includes the real 2% + the 1.5% from the on hit. Close enough.
                # This makes the item better than it is on high crit builds, since the fake 1.5% should not be able to crit.
                # The effect should be minor though.
                titanicExtraAd = 0.035 * bonusHP
                self.ad += titanicExtraAd
                # print(f"titanicExtraAd += {titanicExtraAd}")

            if item.name == "shojin":
                self.hasShojin = True
                shojinHaste = (self.ad - self.noItemAd) * 0.08
                self.haste += shojinHaste
                # print(f"shojinHaste += {shojinHaste}")
            # Item changed in 11.21
            # if item.name == "goredrinker":
            #     goreCoeff = 1.0 + 0.15 * (1 - self.currentPercentHP) / 0.75
            #     self.ad = self.ad * goreCoeff
            #     print("Gore gives {} bonus ad!".format(self.ad - self.ad / goreCoeff))

    def update(self):
        self.updateStatsForLvl(self.lvl)
        self.updateStatsFromItems()

    def aaAverageDamage(self):
        if (self.hasIe) and (self.critChance >= 0.6):
            critMultiplier = 2.25
        else:
            critMultiplier = 1.75

        normalDamage = self.ad
        critDamage = normalDamage * critMultiplier
        critChance = self.critChance
        averageDamage = normalDamage * (1 - critChance) + critDamage * critChance
        totalDamage = averageDamage + self.bonusOnHit
        return totalDamage

    def dpsFromAutos(self, duration=1.0):
        print("as = ", self.as_)
        return (self.aaAverageDamage() + self.bonusOnHit) * self.as_ * duration

    def getDmgFromActions(self, listOfSpells, nbAutos, debugOn=False, sandBag=None):
        dmg = 0
        spellDmg = 0
        autoDmg = 0
        bonusCritDmg = 0
        bonusAd = self.ad - self.noItemAd
        if debugOn:
            print("self.ad = " + str(self.ad))
            print("self.noItemAd = " + str(self.noItemAd))
            print("bonusAd = " + str(bonusAd))
        if (self.hasIe) and (self.critChance >= 0.6):
            bonusCritDmg = 0.5

        for spell in listOfSpells:
            dmg = dmg + spell.getDmgAndCdr(self.ad, bonusAd)[0]
        spellDmg = spellDmg + dmg

        autoDmg = self.aaAverageDamage() * nbAutos

        dmg = dmg + autoDmg
        # TODO take true dmg into account
        trueDmg = 0

        return dmg, spellDmg, autoDmg

    def getAverageDps(self, armorRatio=1.0):
        print("getAverageDps is champion specific, to be overloaded")


class Cait(Champ):
    def __init__(
        self,
        name="Cait",
        lvl=1,
        inventory=[],
        spells=[],
        spellsOrder=[],
        ad0=53.66,
        adPlus=2.18,
        as0=0.543,
        asPlus=0.04,
        hp0=524.4,
        hpPlus=80.0,
        armor0=22.88,
        armorPlus=3.5,
        mr0=30,
        mrPlus=0,
        ms=325,
    ):
        # Overridding Spells :
        P = Spell("P", [0], [1.5], [4], 1)
        Q = Spell(
            "Q", [30, 70, 110, 150, 190], [1.3, 1.4, 1.5, 1.6, 1.7], [10, 9, 8, 7, 6], 0
        )
        W = Spell(
            "W",
            [30, 70, 110, 150, 190],
            [0.7, 0.7, 0.7, 0.7, 0.7],
            [45, 32.5, 20, 12.5, 10],
            0,
        )
        E = Spell(
            "E", [70, 110, 150, 190, 230], [0, 0, 0, 0, 0], [16, 14.5, 13, 11.5, 10], 0
        )
        R = Spell("R", [250, 475, 700], [2.0, 2.0, 2.0], [90, 75, 60], 0)
        spells = []
        spells.append(P)
        spells.append(Q)
        spells.append(W)
        spells.append(E)
        spells.append(R)

        spellsOrder = [1, 3, 1, 2, 1, 4, 1, 1, 2, 2, 4, 2, 2, 3, 3, 3, 3, 4]

        Champ.__init__(
            self,
            name,
            lvl,
            inventory,
            spells,
            spellsOrder,
            ad0,
            adPlus,
            as0,
            asPlus,
            hp0,
            hpPlus,
            armor0,
            armorPlus,
            mr0,
            mrPlus,
            ms,
        )
        # Bonus as :
        self.bonusAs = (
            0.057 * 15 / 10.0
        )  # Cait has a lvl 1 attack speed buff to compensate for her low attack speed multiplier from items. It got changed from +10% to +15% at lvl1 in patch 7.11.

    def getDmgFromActions(self, listOfSpells, nbAutos, debugOn=False, sandBag=None):
        dmg = 0
        spellDmg = 0
        autoDmg = 0
        nbAutosToAdd = 0
        bonusCritDmg = 0
        bonusAd = self.ad - self.noItemAd
        if (self.hasIe) and (self.critChance >= 0.6):
            bonusCritDmg = 0.5

        passiveDmg = self.ad * (0.5 + self.critChance * (1 + 0.5 * bonusCritDmg))

        for spell in listOfSpells:
            if spell.name == "W" or spell.name == "E":
                nbAutosToAdd = nbAutosToAdd + 1
                # Accounting for passive dmg
                dmg = dmg + spell.getDmgAndCdr(self.ad, bonusAd)[0] + passiveDmg
            elif spell.name == "P":
                nbAutosToAdd = nbAutosToAdd + 1
                dmg = dmg + passiveDmg
            else:
                dmg = dmg + spell.getDmgAndCdr(self.ad, bonusAd)[0]
        spellDmg = dmg
        # We need at least nbAutosToAdd to account for the bonus (and bonus only) dmg from the passive
        autoDmg = self.aaAverageDamage() * max(nbAutos, nbAutosToAdd)

        # Taking into account the passive dmg
        autoDmg = autoDmg + passiveDmg * nbAutos / 7.0
        #    print "passive Dmg = ", passiveDmg, ", passiveOnAutosContribution = ", passiveDmg * nbAutos/7.0

        dmg = dmg + autoDmg
        trueDmg = 0

        return dmg, spellDmg, autoDmg, trueDmg

    def specificStatsUpdate(self, lvl):
        None

    def getAverageDps(self, armorRatio=1.0):
        bonusAd = self.ad - self.noItemAd
        critChance = min(1, self.critChance)
        autoDmg = self.as_ * self.ad * (1 + critChance * (0.75)) + self.as_ * (
            self.bonusOnHit
        )
        spellDmg = 0

        dmg = armorRatio * (autoDmg + spellDmg)

        return dmg, spellDmg, autoDmg


class Draven(Champ):
    def __init__(
        self,
        name="Draaaven",
        lvl=1,
        inventory=[],
        spells=[],
        spellsOrder=[],
        ad0=55.8,
        adPlus=2.91,
        as0=0.679,
        asPlus=0.027,
        hp0=557.76,
        hpPlus=82.0,
        armor0=25.544,
        armorPlus=3.3,
        mr0=30,
        mrPlus=0,
        ms=330,
    ):
        # Overridding Spells :
        P = Spell("P", [0], [0], [4], 1)
        Q = Spell(
            "Q",
            [30, 35, 40, 45, 50],
            [0.65, 0.75, 0.85, 0.95, 1.05],
            [12, 11, 10, 9, 8],
            0,
            isBonusRatio=True,
        )
        # TODO implement AS steroids
        W = Spell("W", [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [1, 1, 1, 1, 1], 0)
        E = Spell(
            "E",
            [70, 105, 140, 175, 210],
            [0.5, 0.5, 0.5, 0.5, 0.5],
            [18, 17, 16, 15, 14],
            0,
            isBonusRatio=True,
        )
        R = Spell(
            "R",
            [175 * 2, 275 * 2, 375 * 2],
            [1.1, 1.1, 1.1],
            [120, 100, 80],
            0,
            isBonusRatio=True,
        )
        spells = []
        spells.append(P)
        spells.append(Q)
        spells.append(W)
        spells.append(E)
        spells.append(R)

        spellsOrder = [1, 3, 1, 2, 1, 4, 1, 1, 2, 2, 4, 2, 2, 3, 3, 3, 3, 4]

        Champ.__init__(
            self,
            name,
            lvl,
            inventory,
            spells,
            spellsOrder,
            ad0,
            adPlus,
            as0,
            asPlus,
            hp0,
            hpPlus,
            armor0,
            armorPlus,
            mr0,
            mrPlus,
            ms,
        )


class Tryndamere(Champ):
    # Updated on 29/04/2023
    def __init__(
        self,
        name="Tryndamere",
        lvl=1,
        inventory=[],
        spells=[],
        spellsOrder=[],
        ad0=72,
        adPlus=4.0,
        as0=0.67,
        asPlus=0.029,
        hp0=696,
        hpPlus=115.0,
        armor0=33,
        armorPlus=4.3,
        mr0=32.0,
        mrPlus=2.05,
        ms=345,
    ):
        # Overridding Spells :
        P = Spell("P", [0], [0], [4], 1)
        Q = Spell(
            "Q",
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [12, 12, 12, 12, 12],
            0,
            isBonusRatio=False,
            bonusAdPerLvl=[
                10 + 0,
                10 + 5,
                10 + 10,
                10 + 15,
                10 + 20,
            ],
        )
        W = Spell("W", [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [14, 14, 14, 14, 14], 0)
        E = Spell(
            "E",
            [80, 110, 140, 170, 200],
            [1.3, 1.3, 1.3, 1.3, 1.3],
            [12, 11, 10, 9, 8],
            0,
            isBonusRatio=True,
        )
        R = Spell(
            "R",
            [0, 0, 0],
            [0, 0, 0],
            [120, 100, 80],
            0,
            isBonusRatio=False,
        )
        spells = []
        spells.append(P)
        spells.append(Q)
        spells.append(W)
        spells.append(E)
        spells.append(R)
        # Usual max Q first
        spellsOrder = [3, 1, 1, 2, 1, 4, 1, 1, 3, 3, 3, 3, 2, 2, 2, 2, 4, 4]
        # Special, Q rank 3 then max E
        # spellsOrder = [3, 1, 1, 2, 1, 4, 3, 3, 3, 3, 1, 1, 2, 2, 2, 2, 4, 4]
        # Madness, leaving Q lvl 1. Tested it in simulation, never good.
        # spellsOrder = [3, 1, 3, 2, 3, 4, 3, 3, 1, 1, 1, 1, 2, 2, 2, 2, 4, 4]

        self.rune = "None"  # Options are "None", "Tempo" and "HoB"

        Champ.__init__(
            self,
            name,
            lvl,
            inventory,
            spells,
            spellsOrder,
            ad0,
            adPlus,
            as0,
            asPlus,
            hp0,
            hpPlus,
            armor0,
            armorPlus,
            mr0,
            mrPlus,
            ms,
        )

    def specificStatsUpdate(self, lvl):
        self.critChance = 0.50  # 0.5 as of 14.10 again
        if self.rune == "HoB":
            # Counting 110% AS boost
            self.as_ = self.as_ + 1.1 * self.as0
        elif self.rune == "Tempo":
            # Counting AS boost
            if self.lvl < 3.0:
                extra_tempo = 0.0
            elif self.lvl < 6.0:
                extra_tempo = 0.06
            elif self.lvl < 9.0:
                extra_tempo = 0.12
            elif self.lvl < 12.0:
                extra_tempo = 0.18
            elif self.lvl < 15.0:
                extra_tempo = 0.24
            else:
                extra_tempo = 0.3
            self.as_ = self.as_ + (0.6 + extra_tempo) * self.as0
        elif self.rune == "None":
            None
        else:
            print("ERROR: unkownn rune !")

    def getAverageDps(self, armorRatio=1.0):
        bonusCritDmg = 0
        bonusAd = self.ad - self.noItemAd
        critChance = min(1, self.critChance)
        if (self.hasIe) and (self.critChance >= 0.6):
            bonusCritDmg = 0.5
        eCdrReducer = 1.5
        wrathOnHit = 0
        critActive = 1
        onHitTrueDmg = 0
        trueDmg = 0
        if self.hasGuinsoo:
            eCdrReducer = 0
            wrathOnHit = 200 * critChance
            critActive = 0
        if self.hasRageknife:
            eCdrReducer = 0
            wrathOnHit = 175 * critChance
            critActive = 0
        if self.hasKraken:
            onHitTrueDmg = (0.4 * bonusAd + 50) / 3.0
        if self.hasNavori:
            dmg_mult = 1.0 + self.critChance / 5
            # Patch 13.8, Navori sucks lol
            # print(
            #     f"Ecd with navori passive {calculate_duration_for_E_reset_navori(self.spells[3].cdPerLevel[self.spells[3].lvl - 1], self.as_, self.cdr, self.critChance, has_navori=True)}"
            # )
            # print(
            #     f"Ecd without navori passive {calculate_duration_for_E_reset_navori(self.spells[3].cdPerLevel[self.spells[3].lvl - 1], self.as_, self.cdr, self.critChance, has_navori=False)}"
            # )
        else:
            dmg_mult = 1.0
        Ecd = calculate_duration_for_E_reset_navori(
            self.spells[3].cdPerLevel[self.spells[3].lvl - 1],
            self.as_,
            self.haste,
            self.critChance,
            has_navori=self.hasNavori,
        )

        autoDmg = self.as_ * self.ad * (
            1 + critActive * critChance * (0.75 + bonusCritDmg)
        ) + self.as_ * (self.bonusOnHit + wrathOnHit + onHitTrueDmg)
        spellDmg = (
            dmg_mult
            * (
                self.spells[3].dmgPerLevel[self.spells[3].lvl - 1]
                + self.spells[3].ratioPerLevel[self.spells[3].lvl - 1] * bonusAd
            )
            / Ecd
        )
        trinityDmg = 0
        tiamatDmg = 0
        # TODO Both items changed in S11
        # if self.hasTrinity:
        #     # This assumes perfect trinity passive usage
        #     trinityDmg = self.noItemAd * 2 / 1.5
        #     # print ("trinity extra dps pasive = {}".format(otherDmg))
        # Back again in S14
        if self.hasProfane:
            # Assumes 1 proc
            tiamatDmg = 1.3 * self.ad
        elif self.hasTiamat:
            # Assumes 1 proc of tiamat
            tiamatDmg = 1.0 * self.ad
        if self.hasEssence:
            # This assumes passive usage every 2s
            trinityDmg += (self.noItemAd + 0.4 * bonusAd) / 2.0
        if self.hasStridebreaker:
            trinityDmg += (self.noItemAd * 1.75) / (15.0 * (100 / (100 + self.haste)))
        if self.hasGoredrinker:
            trinityDmg += (self.noItemAd * 1.75) / (15.0 * (100 / (100 + self.haste)))
        if self.hasClaw:
            trinityDmg += (75 + bonusAd * 0.3) / (60.0)

        # Damage formula accounts for AA dmg (normal + crit + IE bonus + trinity passive), on hit, E dmg with cd reductions on cdr and crits
        dmg = armorRatio * (autoDmg + spellDmg + trinityDmg + tiamatDmg) + trueDmg

        return dmg, spellDmg, autoDmg

    def getAverageDpsTurret(self, armorRatio=1.0):
        bonusAd = self.ad - self.noItemAd

        # Most on hit effects don't work on turrets anymore
        autoDmg = self.as_ * self.ad  # + self.as_ * (self.bonusOnHit)

        if self.hasHullbreaker:
            autoDmg = autoDmg * 1.2

        dmg = armorRatio * (autoDmg)

        return dmg

    def hailTradingPattern(self, armorRatio=1.0, opponentHp=None, verbose=False):
        # The goal here is to simulate the (E in + 3 HoB AA trading pattern + E again if it resets) trading pattern.
        if opponentHp is None:
            # We'll assume we're hitting ourselves
            opponentHp = self.hpMax
        bonusCritDmg = 0
        bonusAd = self.ad - self.noItemAd
        fullEDuration = 0.7
        eFullCD = self.spells[3].cdPerLevel[self.spells[3].lvl - 1] * (
            100 / (100 + self.haste)
        )
        windupRatio = 0.19
        aaPeriod = 1 / self.as_
        nbAutos = 3
        # The first of the 3 autos comes out faster then the rest because the auto is already up
        totalDuration = fullEDuration + windupRatio * aaPeriod + 2 * aaPeriod
        critChance = min(1, self.critChance)
        if (self.hasIe) and (self.critChance >= 0.6):
            bonusCritDmg = 0.5
        eCdrReducer = 1.5
        wrathOnHit = 0
        critActive = 1
        onHitTrueDmg = 0
        claw_ratio = 1.0
        if self.hasGuinsoo:
            eCdrReducer = 0
            wrathOnHit = 200 * critChance
            critActive = 0
        if self.hasRageknife:
            eCdrReducer = 0
            wrathOnHit = 175 * critChance
            critActive = 0
        if self.hasKraken:
            onHitTrueDmg = (0.45 * bonusAd + 60) / 3.0
        if self.hasClaw:
            claw_ratio = 1.15

        autoDmgNoCrit = claw_ratio * (
            armorRatio * (self.ad + self.bonusOnHit + wrathOnHit) + onHitTrueDmg
        )
        autoDmgCrit = claw_ratio * (
            armorRatio
            * (
                self.ad * (1 + critActive * (0.75 + bonusCritDmg))
                + self.bonusOnHit
                + wrathOnHit
            )
            + onHitTrueDmg
        )
        eDmg = (
            claw_ratio
            * armorRatio
            * (
                self.spells[3].dmgPerLevel[self.spells[3].lvl - 1]
                + self.spells[3].ratioPerLevel[self.spells[3].lvl - 1] * bonusAd
            )
        )
        trinityDmg = 0
        tiamatDmg = 0

        # TODO both items changed in S11
        # if self.hasTrinity:
        #     # This assumes 1 trinity passive usage
        #     trinityDmg = armorRatio * self.noItemAd * 2 / 1.5
        # Back again in S14
        if self.hasProfane:
            # Assumes 1 proc
            tiamatDmg = 1.3 * self.ad
        elif self.hasTiamat:
            # Assumes 1 proc of tiamat at maximum dmg
            tiamatDmg = armorRatio * 1.0 * self.ad
        if self.hasEssence:
            trinityDmg += armorRatio * (self.noItemAd + 0.4 * bonusAd)
        if self.hasStridebreaker:
            trinityDmg += armorRatio * (self.ad * 1.0)
        if self.hasGoredrinker:
            trinityDmg += armorRatio * (self.ad * 1.0)
        if self.hasClaw:
            trinityDmg += armorRatio * (65 + bonusAd * 0.25)

        otherDmg = trinityDmg + tiamatDmg
        header = [
            "Nb crits",
            "Proba",
            "Nb E",
            "Total dmg",
            "AA dmg",
            "E dmg",
            "Other dmg",
            "Duration",
            "DPS",
            "CD to next E",
            "% of opponent HP",
        ]
        results = []
        expectedDmg = 0
        expectedSpellDmg = 0
        expectedAutoDmg = 0
        expectednbE = 0
        for nbCrits in range(0, nbAutos + 1):
            # There will be between 0 and 3 crits
            k = nbCrits
            n = nbAutos
            p = critChance
            proba = scipy.special.comb(n, k) * math.pow(p, k) * math.pow(1 - p, n - k)
            nbE = 1
            remainingECd = 0
            navoriCoef = 1.0
            if self.hasNavori:
                navoriCoef = 0.88

            # As of patch 12.22 navori calculation became easier.
            # We'll assume that if 1 crit then it's the second AA, if 2 crits it's the second and third AA (average-pessimistic)
            if nbCrits == 0:
                isCrits = [0, 0, 0]
            elif nbCrits == 1:
                isCrits = [0, 1, 0]
            elif nbCrits == 2:
                isCrits = [0, 1, 1]
            else:
                isCrits = [1, 1, 1]
            # Full E + AA animation for the first auto
            remainingECd = eFullCD - fullEDuration - windupRatio * aaPeriod
            # We'll assume that if an E is used after the first one, it's with no length (instant E in place for dmg)
            for isCrit in isCrits:
                # Reducing E CD by 2 secs when only 0.5 sec was left will "waste" 1.5s of reduction
                remainingECd = max(0, remainingECd * navoriCoef - isCrit * eCdrReducer)
                if remainingECd == 0:
                    # The CD reduction on crit reseted E
                    nbE += 1
                    remainingECd = eFullCD
                # Waiting for next auto
                remainingECd -= aaPeriod
                if remainingECd < 0:
                    # Waiting for the next auto reseted E
                    nbE += 1
                    remainingECd = remainingECd + eFullCD
            # Previous method used was less precise and couldn't account for Navori
            # nbE_old = 1 + math.floor(
            #     (fullEDuration + totalDuration + nbCrits * eCdrReducer) / eFullCD
            # )
            # remainingECd_old = eFullCD * (nbE) - (
            #     totalDuration + nbCrits * eCdrReducer + fullEDuration
            # )
            # print (
            #     "nbE={}, old={},  remainingECd={}, old={}".format(
            #         nbE, nbE_old, remainingECd, remainingECd_old
            #     )
            # )

            autoDmg = nbCrits * autoDmgCrit + (nbAutos - nbCrits) * autoDmgNoCrit
            spellDmg = nbE * eDmg
            # Damage formula accts for AA dmg (normal + crit + IE bonus + trinity passive), on hit, E dmg with cd reductions on cdr and crits
            dmg = autoDmg + spellDmg + otherDmg
            percentHpDmg = dmg / opponentHp
            expectedDmg += proba * dmg
            expectedSpellDmg += proba * spellDmg
            expectedAutoDmg += proba * autoDmg
            expectednbE += proba * nbE
            results.append([])
            results[nbCrits] = [
                nbCrits,
                100 * proba,
                nbE,
                dmg,
                autoDmg,
                spellDmg,
                otherDmg,
                totalDuration,
                dmg / totalDuration,
                remainingECd,
                100 * percentHpDmg,
            ]
        # Used for debug
        # if (
        #     containsItem(self.inventory, "cloak")
        #     and containsItem(self.inventory, "cloak")
        #     and containsItem(self.inventory, "IE")
        #     and containsItem(self.inventory, "ionanBoots")
        #     and len(self.inventory) == 4
        # ):
        #     verbose = True
        if verbose:
            print("")
            print(
                "lvl={}, crit chance={}, E CD={}, Haste={}, keystone={}, shard1={}, inventory={}, armorRatio={:.2f}".format(
                    self.lvl,
                    critChance,
                    eFullCD,
                    self.haste,
                    self.rune,
                    self.minorRune1,
                    self.inventory,
                    armorRatio,
                )
            )
            print(tabulate(results, headers=header, tablefmt="", floatfmt=".1f"))
            print("Damage on average={:.1f}".format(expectedDmg))
            # toBeSaved = tabulate(data, headers=header, tablefmt="github")

        return (
            header,
            results,
            expectedDmg,
            expectedSpellDmg,
            expectedAutoDmg,
            expectednbE,
        )


class Nasus(Champ):
    def __init__(
        self,
        name="Nasus",
        lvl=1,
        inventory=[],
        spells=[],
        spellsOrder=[],
        ad0=67,
        adPlus=3.5,
        as0=0.638,
        asPlus=0.0348,
        hp0=561,
        hpPlus=90,
        armor0=34,
        armorPlus=3.5,
        mr0=32,
        mrPlus=1.25,
        ms=350,
    ):
        # Overridding Spells (TODO? Needs a mechanism to activate R)

        self.ult_on = False

        Champ.__init__(
            self,
            name,
            lvl,
            inventory,
            spells,
            spellsOrder,
            ad0,
            adPlus,
            as0,
            asPlus,
            hp0,
            hpPlus,
            armor0,
            armorPlus,
            mr0,
            mrPlus,
            ms,
        )

    def specificStatsUpdate(self, lvl):
        if self.ult_on:
            if lvl >= 16:
                self.hpMax += 600
                self.hp += 600
                self.armor += 70
                self.mr += 70
            elif lvl >= 11:
                self.hpMax += 450
                self.hp += 450
                self.armor += 55
                self.mr += 55
            elif lvl >= 6:
                self.hpMax += 300
                self.hp += 300
                self.armor += 40
                self.mr += 40
