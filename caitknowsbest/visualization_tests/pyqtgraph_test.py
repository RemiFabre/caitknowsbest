import sys
import numpy as np
import pandas as pd
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QVBoxLayout,
    QTableView,
    QWidget,
    QSplitter,
)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItemModel, QStandardItem
import pyqtgraph as pg


def create_model(df):
    model = QStandardItemModel(df.shape[0], df.shape[1])
    model.setHorizontalHeaderLabels(df.columns)
    for i, index in enumerate(df.index):
        for j, col in enumerate(df.columns):
            item = QStandardItem(f"{df.loc[index, col]:.1f}")
            item.setTextAlignment(Qt.AlignCenter)
            model.setItem(i, j, item)
    return model


# Create a 20x18 matrix with random numbers
data = np.random.rand(18, 20)
columns = [f"Column_{i+1}" for i in range(20)]
df = pd.DataFrame(data, columns=columns)

app = QApplication(sys.argv)
main_window = QMainWindow()

# Create DataFrame table view
table_view = QTableView()
table_model = create_model(df)
table_view.setModel(table_model)
table_view.resizeColumnsToContents()
table_view.horizontalHeader().setStretchLastSection(True)

# Create the plot
plot_widget = pg.PlotWidget()
plot_widget.setLabel("left", "Values")
plot_widget.setLabel("bottom", "Rows")
plot_widget.setTitle("Example Plot")

for i, column in enumerate(df.columns):
    plot_widget.plot(
        df.index,
        df[column],
        symbol="o",
        pen=pg.mkPen(color=(i * 20, (i + 5) * 20, 255 - i * 20), width=1.5),
        name=column,
    )

# Layout
splitter = QSplitter(Qt.Vertical)
splitter.addWidget(table_view)
splitter.addWidget(plot_widget)

central_widget = QWidget()
layout = QVBoxLayout()
layout.addWidget(splitter)
central_widget.setLayout(layout)

main_window.setCentralWidget(central_widget)
main_window.resize(1000, 600)
main_window.setWindowTitle("DataFrame Viewer with Plot")
main_window.show()

sys.exit(app.exec_())
