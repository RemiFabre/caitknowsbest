import tkinter as tk
from pandastable import Table
import pandas as pd
import numpy as np

# Create a 20x18 matrix with random numbers
data = np.random.rand(18, 20)
columns = [f"Column_{i+1}" for i in range(20)]
df = pd.DataFrame(data, columns=columns)

# Create the main Tkinter window
root = tk.Tk()
root.title("DataFrame Viewer")

# Create a Frame to hold the Table widget
frame = tk.Frame(root)
frame.pack(fill="both", expand=True)

# Create the Table widget to display the DataFrame
pt = Table(frame, dataframe=df, showtoolbar=True, showstatusbar=True)
pt.show()

# Start the Tkinter event loop
root.mainloop()