import sys
from PyQt5.QtWidgets import QApplication, QTableView
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItemModel, QStandardItem
import pandas as pd
import numpy as np


def create_model(df):
    model = QStandardItemModel(df.shape[0], df.shape[1])
    model.setHorizontalHeaderLabels(df.columns)
    for i, index in enumerate(df.index):
        for j, col in enumerate(df.columns):
            item = QStandardItem(f"{df.loc[index, col]:.1f}")
            item.setTextAlignment(Qt.AlignCenter)
            model.setItem(i, j, item)
    return model


# Create a 20x18 matrix with random numbers
data = np.random.rand(18, 20)
columns = [f"Column_{i+1}" for i in range(20)]
df = pd.DataFrame(data, columns=columns)

app = QApplication(sys.argv)
view = QTableView()

model = create_model(df)
view.setModel(model)
view.resizeColumnsToContents()
view.horizontalHeader().setStretchLastSection(True)
view.show()

sys.exit(app.exec_())
