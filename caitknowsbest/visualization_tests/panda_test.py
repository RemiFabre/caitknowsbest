import pandas as pd
import numpy as np

# Set the display options for Pandas
pd.options.display.float_format = "{:.1f}".format
pd.set_option("display.max_columns", None)
pd.set_option("display.width", None)

# Create a 20x18 matrix with random numbers
data = np.random.rand(18, 20)

# Create a DataFrame with column names
columns = [f"Column_{i+1}" for i in range(20)]
df = pd.DataFrame(data, columns=columns)

print(df)