import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import matplotlib.cm as cm


def plot_importance(ax, dictionary, title):
    sns.set_theme()  # Apply Seaborn's styling
    df = pd.DataFrame(list(dictionary.items()), columns=["Stat", "Importance"])

    sns.barplot(x="Importance", y="Stat", data=df, palette="viridis", ax=ax)
    ax.set_title(title)
    ax.set_xlabel("Importance")
    ax.set_ylabel("Stats")


def plot_radar(ax, dictionary, title, color="blue"):
    df = pd.DataFrame(list(dictionary.items()), columns=["Stat", "Importance"])

    # Normalize the data
    max_value = df["Importance"].max()
    df["Normalized"] = df["Importance"] / max_value

    num_vars = len(df)
    angles = np.linspace(0, 2 * np.pi, num_vars, endpoint=False).tolist()
    angles += angles[:1]  # Close the plot

    values = df["Normalized"].tolist()
    values += values[:1]  # Close the plot

    ax.plot(angles, values, linewidth=2, linestyle="solid", marker="o", markersize=5, color=color)
    ax.fill(angles, values, alpha=0.25)

    ax.set_title(title, size=14, color=color, y=1.1)
    ax.set_xticks(angles[:-1])
    ax.set_xticklabels(df["Stat"])
    ax.set_yticklabels([])  # Hide y-axis labels
    ax.set_rlabel_position(30)  # Position of the radial labels
    ax.set_ylim(0, 1)


def plot_donut(dictionary, title):
    df = pd.DataFrame(list(dictionary.items()), columns=["Stat", "Importance"])

    fig, ax = plt.subplots(figsize=(6, 6))
    cmap = plt.get_cmap("viridis")
    colors = [cmap(i) for i in np.linspace(0, 1, len(df))]
    wedges, texts, _ = ax.pie(df["Importance"], labels=df["Stat"], colors=colors, autopct="%1.1f%%", startangle=90)
    ax.set_title(title)
    ax.axis("equal")  # Equal aspect ratio ensures that pie is drawn as a circle.

    # Draw a white circle at the center to create a donut chart
    center_circle = plt.Circle((0, 0), 0.6, fc="white")
    fig.gca().add_artist(center_circle)

    plt.show()


if __name__ == "__main__":
    dict1 = {"Stat1": 0.8, "Stat2": 0.5, "Stat3": 0.6, "Stat4": 0.3, "Stat5": 0.7, "Stat6": 0.1, "Stat7": 0.9, "Stat8": 0.4}
    dict2 = {"Stat1": 0.4, "Stat2": 0.7, "Stat3": 0.9, "Stat4": 0.3, "Stat5": 0.5, "Stat6": 0.2, "Stat7": 0.6, "Stat8": 0.8}
    dict3 = {"Stat1": 0.5, "Stat2": 0.6, "Stat3": 0.4, "Stat4": 0.9, "Stat5": 0.1, "Stat6": 0.7, "Stat7": 0.8, "Stat8": 0.2}

    fig, axes = plt.subplots(1, 3, figsize=(18, 6), subplot_kw=dict(polar=True))
    plot_radar(axes[0], dict1, "Importance of Stats in Dictionary 1")
    plot_radar(axes[1], dict2, "Importance of Stats in Dictionary 2")
    plot_radar(axes[2], dict3, "Importance of Stats in Dictionary 3")
    plt.tight_layout()
    plt.show()

    fig, axes = plt.subplots(1, 3, figsize=(18, 4))
    plot_importance(axes[0], dict1, "Importance of Stats in Dictionary 1")
    plot_importance(axes[1], dict2, "Importance of Stats in Dictionary 2")
    plot_importance(axes[2], dict3, "Importance of Stats in Dictionary 3")
    plt.tight_layout()
    plt.show()

    # plot_donut(dict1, "Importance of Stats in Dictionary 1")
    # plot_donut(dict2, "Importance of Stats in Dictionary 2")
    # plot_donut(dict3, "Importance of Stats in Dictionary 3")

    # plot_radar0(dict1, "Importance of Stats in Dictionary 1")
    # plot_radar0(dict2, "Importance of Stats in Dictionary 2")
    # plot_radar0(dict3, "Importance of Stats in Dictionary 3")
