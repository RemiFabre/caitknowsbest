# -*- coding: utf-8 -*-
# Author : Rémi Fabre
from tabulate import tabulate

nbCallsCombi = 0
nbCallsCombi2 = 0


class InputDps:
    """Utility class to store the incoming dps profile values. Contains true, physical and magical dps."""

    def __init__(self, dps_true, dps_physical, dps_magical):
        self.dps_true = dps_true
        self.dps_physical = dps_physical
        self.dps_magical = dps_magical


def getGoldForLvl(lvl):
    # This seemed a bit high. Maybe because it was from an adc carry game? (lower exp for same amount of gold)
    # return lvl * lvl * 34.6 + lvl * 224.9 + 500
    return 524 - 91 * lvl + 77.5 * lvl**2 - 5.69 * lvl**3 + 0.241 * lvl**4


def inventoryCost(items):
    cost = 0
    for i in items:
        cost = cost + i.totalCost
    return cost


def sort_dict(input_dict):
    """Returns a list of tuples corresponding to the input_dict, sorted by value."""
    return sorted(input_dict.items())


def removeInvDuplicates(input):
    output = []
    for inv in input:
        for inv2 in output:
            isDuplicate = False
            if len(inv) == len(inv2):
                for i in range(len(inv)):
                    if inv[i].name != inv2[i].name:
                        # No match
                        break
                    isDuplicate = True
                    break
            if isDuplicate:
                continue
        # Not a duplicate
        output.append(inv)
    return output


def calculate_duration_for_E_reset_navori(E0, AS, haste, crit_chance, has_navori=False):
    """Tryndamere specific function to calculate the duration of an E reset with Navori Quickblades."""
    # E0 = 8
    aa_period = 1 / AS
    Ecd = E0 * 100 / (100 + haste)
    last_Ecd = Ecd
    i = 0
    while True:
        # We waited for 1 auto period, updating Ecd
        i += 1
        last_Ecd = Ecd
        naturalEcd = Ecd - aa_period
        if has_navori:
            Ecd = Ecd * 0.85 - 1.5 * crit_chance - aa_period
        else:
            Ecd = Ecd - 1.5 * crit_chance - aa_period

        # print(f"Ecd: {Ecd}, naturalEcd: {naturalEcd}")
        # Calculating the remaining CD after 1 auto period
        if Ecd < 0:
            # We overshooted, the real answer is between i*aa_period and (i-1)*aa_period
            if (naturalEcd) < 0:
                # The last auto was not needed to reset E
                return (i - 1) * aa_period + last_Ecd
            else:
                # The last auto was needed to reset E
                return i * aa_period


def compareInventories(
    champ, sandBag, listOfInventories, listOfSpells, fightDuration=5
):
    i = 0
    for inventory in listOfInventories:
        cost = 0
        lethality = 0
        baseArmor = sandBag.armor
        output = []
        print("Btw, tower as = 0.83")
        output.append("\nTrying inventory {id} : ".format(id=i))
        for item in inventory:
            output.append("{name}, ".format(name=item.name))
            cost = cost + item.totalCost
            lethality = lethality + item.bonusLethality
        output.append("cost = {value}".format(value=cost))
        print("".join(output))
        champ.inventory = inventory
        champ.update()
        nbAutos = fightDuration * champ.as_
        dmg, spellDmg, autoDmg = champ.getDmgFromActions(listOfSpells, nbAutos)
        print(
            "dmg = ",
            dmg,
            ", spellDmg = ",
            spellDmg,
            ", autoDmg = ",
            autoDmg,
            ", nbAutos = ",
            nbAutos,
            ", as = ",
            champ.as_,
        )
        arPen = lethality  # lethality * (0.6 + 0.4 * champ.lvl / 18.0)
        sandBagArmor = max(0, baseArmor - arPen) * (1 - champ.percentArmorPen)
        dmg = dmg * (100.0 / (100 + sandBagArmor))
        spellDmg = spellDmg * (100.0 / (100 + sandBagArmor))
        autoDmg = autoDmg * (100.0 / (100 + sandBagArmor))
        print(
            "*(after arPen) dmg = ",
            dmg,
            ", spellDmg = ",
            spellDmg,
            ", autoDmg = ",
            autoDmg,
            ", nbAutos = ",
            nbAutos,
            ", armor = ",
            sandBagArmor,
        )
        i = i + 1


def combiSearch(resultList, maxValuePerSlot, sumGoal, currentList=[]):
    """Updates resultList (type list of lists) so that it contains all the viable combinations of len(maxValuePerSlot) numbers whose sum is sumGoal.
    e.g combiSearch([], resultList, [2,3], 4) would update resultList to [[2,2],[1,3]]

    Args:
        resultList (list of lists of int): Start with []
        maxValuePerSlot (list of int): Maximum allowed value for each slot. Also used to specify the number of slots (=numbers)
        sumGoal (int): The sum of values will sum to sumGoal
        currentList (list of int): Start with []
    """
    global nbCallsCombi
    nbCallsCombi += 1
    if len(currentList) == len(maxValuePerSlot):
        # A complete solution has been found, saving and exiting
        resultList.append(currentList)
        return
    currentSum = 0
    for i in currentList:
        currentSum += i
    maxValue = min(sumGoal - currentSum, maxValuePerSlot[len(currentList)])
    if maxValue < 0:
        # This combination can't work
        return
    if len(currentList) == (len(maxValuePerSlot) - 1):
        # This is the last number so there is only 1 viable value
        value = sumGoal - currentSum
        if value <= maxValue:
            # This works
            newList = currentList[:]
            newList.append(value)
            return combiSearch(resultList, maxValuePerSlot, sumGoal, newList)
        else:
            # This combination can't work
            return
    for i in range(maxValue + 1):
        # Exploring all of the viable numbers for this slot
        newList = currentList[:]
        newList.append(i)
        combiSearch(resultList, maxValuePerSlot, sumGoal, newList)


# by cduss more concise and less calls? What a man
def combiSearch2(maxValuePerSlot, sumGoal, currentList=[]):
    """Updates resultList (type list of lists) so that it contains all the viable combinations of len(maxValuePerSlot) numbers whose sum is sumGoal.
    e.g combiSearch([], resultList, [2,3], 4) would update resultList to [[2,2],[1,3]]
    Args:
        currentList (list of lists of int): Start with []
        resultList (list of int): Start with []
        maxValuePerSlot (list of int): Maximum allowed value for each slot. Also used to specify the number of slots (=numbers)
        sumGoal (int): The sum of values will sum to sumGoal
    """
    global nbCallsCombi2
    nbCallsCombi2 += 1
    currentSlot = len(currentList)
    if currentSlot == len(maxValuePerSlot) - 1:
        return [currentList + [sumGoal]] if sumGoal <= maxValuePerSlot[-1] else None
    local_list = []
    for i in range(0, maxValuePerSlot[currentSlot] + 1):
        if sumGoal >= i:
            res = combiSearch2(maxValuePerSlot, sumGoal - i, currentList + [i])
            if res is not None:
                local_list = local_list + res
    return local_list


def printResults(results, champ, header, sortIndexes=[0], limitResults=None):
    # Super non efficient way of doing it
    for sortIndex in sortIndexes:
        # Sorting the results by a column
        results.sort(key=lambda x: x[sortIndex], reverse=True)
        nbResults = len(results)
        knownInvs = []
        filteredResults = []
        # This is very heavy computational wise. A brutal way of doing it is deleting dmg duplicates...
        for r in results:
            # Sorting alphabetically each inv and creating a defining string
            invStr = []
            for i in r[2]:
                invStr.append(i.name)
            invStr = sorted(invStr)
            # print ("invStr = {}".format(invStr))
            definingInvString = "".join(invStr)
            # print (definingInvString)
            if not (definingInvString in knownInvs):
                knownInvs.append(definingInvString)
                filteredResults.append(r)

        if limitResults is not None:
            filteredResults = filteredResults[: min(limitResults, len(filteredResults))]
        # filteredResults = results
        print(
            tabulate(filteredResults, headers=header, tablefmt="github", floatfmt=".2f")
        )
        print(
            "{} shown builds (initially had {} builds). Sorted by {}".format(
                len(filteredResults), nbResults, header[sortIndex]
            )
        )

        # for r in filteredResults:
        #     print (r)

        # # This resets the champions stats (that were modified by items), just to print them
        champ.updateStatsForLvl(champ.lvl)
        print(
            "\n without items **Lvl : "
            + str(champ.lvl)
            + "**, baseAd = "
            + "{0:.1f}".format(champ.ad)
            + ", baseAs = "
            + "{0:.2f}".format(champ.as_)
            + ", percentArmorPen = "
            + "{0:.2f}".format(champ.percentArmorPen)
            + ", critChance = "
            + "{0:.2f}".format(champ.critChance)
            + ", haste = "
            + "{0:.2f}".format(champ.haste)
            + "\n***********************************************\n\n"
        )


if __name__ == "__main__":
    print("Testing utils.py")
    resultList = []
    combiSearch(resultList, [2, 3], 4)
    print("combiSearch(resultList, [2,3], 4)= {}".format(resultList))
    print("\n#####################################################\n")
    print("combiSearch2([2, 3], 4)= {}".format(combiSearch2([2, 3], 4)))
    print("\n#####################################################\n")
    # resultList = []
    # combiSearch(resultList, [2, 2, 2], 4)
    # print("combiSearch(resultList, [2,2,2], 4)= {}".format(resultList))
    # print("\n#####################################################\n")
    # resultList = []
    # combiSearch(resultList, [2, 10, 10, 10, 10], 10)
    # print("combiSearch(resultList, [2, 10, 10, 10, 10], 10)= {}".format(resultList))
    # print("nb results = {}".format(len(resultList)))
    # print("\n#####################################################\n")
    resultList = []
    combiSearch(resultList, [2, 10, 10, 10, 0, 10], 10)
    print("combiSearch(resultList, [2, 10, 10, 10, 0,  10], 10)= {}".format(resultList))
    print("nb results = {}".format(len(resultList)))
    resultList2 = combiSearch2([2, 10, 10, 10, 0, 10], 10)
    print("combiSearch2([2, 3], 4)= {}".format(resultList2))
    print("\n#####################################################\n")
    print(resultList == resultList2)
    print("len(resultList)  {}".format(len(resultList)))
    print("len(resultList2)  {}".format(len(resultList2)))
    for e in resultList:
        if not (e in resultList2):
            print(e)

    print("nbCallsCombi {}".format(nbCallsCombi))
    print("nbCallsCombi2 {}".format(nbCallsCombi2))
    # TODO why is there less calss in combi2?
