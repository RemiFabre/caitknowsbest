# -*- coding: utf-8 -*-
# Author : Rémi Fabre

import sys
from utils import (
    getGoldForLvl,
    inventoryCost,
    sort_dict,
    removeInvDuplicates,
    printResults,
    compareInventories,
)
from champ import *
from spell import *
from item import *
from items import *
from optimization import idealBuyForLvlVsChampCombi, idealItemsForLvlVsChampCombi
from tryndamere_optimization import (
    tryndamereItemOptimization,
    tryndamereCalculation,
    tryndamereCompareInventories,
)
import copy
import utils
import argparse
from item import containsItem
from argparse import RawTextHelpFormatter
import tryndamere_inventory_tests

# TODO long term: create a simulator of a fight, including spells CDs and all that.

if __name__ == "__main__":
    print("A new day dawns for Cait")
    parser = argparse.ArgumentParser(
        formatter_class=RawTextHelpFormatter,
        description="Various optimizations available : ideal stats, ideal items, inventory comparison. Usage example: \npython3 main.py --lvl 9 --hp 0.01 --dps-mode\npython3 main.py --lvl 18 --hp 1.0 --hob-mode",
    )

    parser.add_argument(
        "--lvl",
        type=int,
        help="If present, only this lvl will be considered",
        default=0,
    )
    parser.add_argument(
        "--hp",
        type=float,
        help="Sets the current hp. 0.5 means that the champion is at half health.",
        default=0.5,
    )

    parser.add_argument(
        "--hob-mode",
        dest="hob",
        action="store_true",
        help="If present, uses the HoB exact trading function (choose this or --dps-mode)",
    )
    parser.add_argument(
        "--dps-mode",
        dest="hob",
        action="store_false",
        help="If present, uses the average damage calculation function (choose this or --hob-mode)",
    )
    parser.set_defaults(hob=False)
    args = parser.parse_args()
    # dravenCalculation()
    # dravenItemOptimization()

    # tryndamereCalculation(bruiserMode=True)
    # sys.exit()

    for i in range(1, 19):
        print("Expected gold for lvl {} is {:.0f} gold".format(i, getGoldForLvl(i)))

    # tryndamereItemOptimization(
    #     force_lvl=args.lvl,
    #     hobMode=args.hob,
    #     rune="Tempo",
    #     minorRune1="AS",
    #     minorRune2="AD",
    #     hasTranscendence=False,
    #     hasCosmic=False,
    #     isTiamatFirst=False,
    #     currentPercentHP=args.hp
    # )
    # sys.exit()
