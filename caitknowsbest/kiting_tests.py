import argparse

"""So an auto attack seems to be:
1) Wind-up
2) 2 frames buffer (that end at the end of the actual windup time) where requesting a movement is delayed until the end of the wind-up time.
3) End animation (cancelable at ay time without any penalty). The duration of this animation is unkown for now. We'll assume it equals the wind-up time for now
4) Free time


"""


def info(attack_freq=1.0, opponent_ms=350):
    attack_period = 1.0 / attack_freq
    windup = 0.19
    frame_duration = 1 / 30.0
    windup_duration = windup * attack_period
    animation_duration = windup * attack_period
    worst_movement_time = attack_period - windup_duration - animation_duration
    best_movement_time = attack_period - windup_duration
    worst_distance_traveled_during_immobility = opponent_ms * (
        windup_duration + animation_duration
    )
    best_distance_traveled_during_immobility = opponent_ms * (windup_duration)
    worst_ms_needed_to_catchup = worst_distance_traveled_during_immobility / float(
        worst_movement_time
    )
    best_ms_needed_to_catchup = best_distance_traveled_during_immobility / float(
        best_movement_time
    )
    print(
        "Perfect kitting information for an attack speed of {:.2f}, a wind-up of {:.2f}% ({:.2f} ms), an end animation of {:.2f}% ({:.2f} ms)".format(
            attack_freq,
            windup,
            1000 * windup_duration,
            windup,
            1000 * animation_duration,
        )
    )
    print(
        "For an optimal kitting, the movement command must be input after {:.2f}ms and before {:.2f}ms".format(
            1000 * (windup_duration - 2 * frame_duration), 1000 * windup_duration
        )
    )
    print(
        "With no kiting, the champion has {:.2f}ms of movement ({:.2f}% of total time)".format(
            1000 * worst_movement_time, 100 * worst_movement_time / attack_period
        )
    )
    print(
        "With perfect kiting, the champion has {:.2f}ms of movement ({:.2f}% of total time)".format(
            1000 * best_movement_time, 100 * best_movement_time / attack_period
        )
    )
    print(
        "With no kitting, the opponent moves {:.1f} units during our immobile time (with a ms of {}). The extra ms needed in order to catch up in time (with no dps decrease) is {:.1F}".format(
            worst_distance_traveled_during_immobility,
            opponent_ms,
            worst_ms_needed_to_catchup,
        )
    )
    print(
        "With pefect kitting, the opponent moves {:.1f} units during our immobile time (with a ms of {}). The extra ms needed in order to catch up in time (with no dps decrease) is {:.1F}".format(
            best_distance_traveled_during_immobility,
            opponent_ms,
            best_ms_needed_to_catchup,
        )
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Auto-attack related tests")
    parser.add_argument(
        "attack_speed",
        type=float,
        help="If present, will define the attack speed fo the calculation",
        default=1.0,
    )
    args = parser.parse_args()
    info(args.attack_speed)
