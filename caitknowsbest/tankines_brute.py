# Interesting chart at : https://leagueoflegends.fandom.com/wiki/Armor?file=HP_Armor_LW_VS.png


def tankiness(hp, armor, mr, dps_true, dps_armor, dps_mr):
    d_total = dps_true + dps_armor * (100) / (100 + armor) + dps_mr * (100) / (100 + mr)
    t = hp / d_total
    return t


def ideal_buy(
    hp,
    armor,
    mr,
    dps_true,
    dps_armor,
    dps_mr,
    total_gold=1000,
    nb_steps=10,
    verbose=True,
):
    step = total_gold / nb_steps
    armor_value = 20
    mr_value = 18
    hp_value = 400 / 150.0
    iters = 0

    ideal_hp = 0
    ideal_armor = 0
    ideal_mr = 0
    ideal_t = -1

    t_base = tankiness(hp, armor, mr, dps_true, dps_armor, dps_mr)

    print("Stats without items HP {:.2f}, base armor={:.2f}, base mr={:.2f}".format(hp, armor, mr))
    print("Receiving a dps of {:.2f} true dmg/s, {:.2f} physical dmg/s, {:.2f} magic dmg/s".format(dps_true, dps_armor, dps_mr))
    print("With no items the tankiness is {:.2f}s".format(t_base))

    # Searching the entire space
    gold = total_gold
    for hp_buys in range(nb_steps + 1):
        local_spent = hp_buys * step
        extra_hp = local_spent / hp_value
        gold = gold - local_spent
        available_steps = nb_steps - hp_buys
        for armor_buys in range(available_steps + 1):
            local_spent = armor_buys * step
            extra_armor = local_spent / armor_value
            gold = gold - local_spent
            available_steps = nb_steps - hp_buys - armor_buys
            # We spend the rest on MR
            local_spent = available_steps * step
            extra_mr = local_spent / mr_value
            t = tankiness(
                hp + extra_hp,
                armor + extra_armor,
                mr + extra_mr,
                dps_true,
                dps_armor,
                dps_mr,
            )
            if verbose:
                print(
                    "Tankiness={:.2f}s, hp={:.2f}(+{:.2f}), armor={:.2f}(+{:.2f}), mr={:.2f}(+{:.2f})".format(
                        t,
                        hp + extra_hp,
                        extra_hp,
                        armor + extra_armor,
                        extra_armor,
                        mr + extra_mr,
                        extra_mr,
                    )
                )
            if t > ideal_t:
                ideal_t = t
                ideal_hp = extra_hp
                ideal_armor = extra_armor
                ideal_mr = extra_mr
    print(
        "The optimal usage of {:.1f} gold, is to buy {:.0f} HP ({:.1f}%), {:.1f} armor ({:.1f}%), {:.1f} mr ({:.1f}%)".format(
            total_gold,
            ideal_hp,
            100 * ideal_hp * hp_value / total_gold,
            ideal_armor,
            100 * ideal_armor * armor_value / total_gold,
            ideal_mr,
            100 * ideal_mr * mr_value / total_gold,
        )
    )
    print("Optimal tankiness is {:.2f}s (+{:.2f}s, +{:.2f}% extra tankiness)\n".format(t, t - t_base, (-1 + t / t_base) * 100))
    return ideal_hp, ideal_armor, ideal_mr, ideal_t


if __name__ == "__main__":

    print("**** Testing against ONLY PHYSICAL damage at different stages in the game ****\n")
    print("Nasus lvl 1 ")
    ideal_buy(561, 34, 32, 0, 100, 0, total_gold=1000, nb_steps=10, verbose=False)
    print("Nasus lvl 5 ")
    ideal_buy(839.1, 44.82, 35.86, 0, 100, 0, total_gold=1000, nb_steps=10, verbose=False)
    print("Nasus lvl 6 ")
    ideal_buy(916.5, 47.83, 36.94, 0, 100, 0, total_gold=1000, nb_steps=10, verbose=False)
    print("Nasus lvl 6 (Ult ON) ")
    ideal_buy(
        916.5 + 300,
        47.83 + 40,
        36.94 + 40,
        0,
        100,
        0,
        total_gold=1000,
        nb_steps=10,
        verbose=False,
    )
    print("Nasus lvl 9 ")
    ideal_buy(1167.6, 57.59, 40.42, 0, 100, 0, total_gold=1000, nb_steps=10, verbose=False)
    print("Nasus lvl 9 (Ult ON) ")
    ideal_buy(
        1167.6 + 300,
        57.59 + 40,
        40.42 + 40,
        0,
        100,
        0,
        total_gold=1000,
        nb_steps=10,
        verbose=False,
    )
    print("Nasus lvl 11 vs ")
    ideal_buy(1350.75, 64.71, 42.97, 0, 100, 0, total_gold=1000, nb_steps=10, verbose=False)
    print("Nasus lvl 11 (Ult ON) ")
    ideal_buy(
        1350.75 + 450,
        64.71 + 55,
        42.97 + 55,
        0,
        100,
        0,
        total_gold=1000,
        nb_steps=10,
        verbose=False,
    )
    print("Nasus lvl 18 ")
    ideal_buy(2091, 93.5, 53.25, 0, 100, 0, total_gold=1000, nb_steps=10, verbose=False)
    print("Nasus lvl 18 (Ult ON) ")
    ideal_buy(
        2091 + 600,
        93.5 + 70,
        53.25 + 70,
        0,
        100,
        0,
        total_gold=1000,
        nb_steps=10,
        verbose=False,
    )

    print("**** Late game with varied damage distributions ****\n")
    print("Nasus lvl 18 (Ult ON) ")
    ideal_buy(
        2091 + 600,
        93.5 + 70,
        53.25 + 70,
        0,
        0,
        100,
        total_gold=1000,
        nb_steps=10,
        verbose=False,
    )
    print("Nasus lvl 18 (Ult ON) ")
    ideal_buy(
        2091 + 600,
        93.5 + 70,
        53.25 + 70,
        100,
        0,
        0,
        total_gold=1000,
        nb_steps=10,
        verbose=False,
    )
    print("Nasus lvl 18 (Ult ON) ")
    ideal_buy(
        2091 + 600,
        93.5 + 70,
        53.25 + 70,
        0,
        50,
        50,
        total_gold=1000,
        nb_steps=10,
        verbose=False,
    )
    print("Nasus lvl 18 (Ult ON) ")
    ideal_buy(
        2091 + 600,
        93.5 + 70,
        53.25 + 70,
        0,
        75,
        25,
        total_gold=1000,
        nb_steps=10,
        verbose=False,
    )
    print("Nasus lvl 18 (Ult ON) ")
    ideal_buy(
        2091 + 600,
        93.5 + 70,
        53.25 + 70,
        0,
        25,
        75,
        total_gold=1000,
        nb_steps=10,
        verbose=False,
    )
    print("Nasus lvl 18 (Ult ON) ")
    ideal_buy(
        2091 + 600,
        93.5 + 70,
        53.25 + 70,
        10,
        45,
        45,
        total_gold=1000,
        nb_steps=10,
        verbose=False,
    )
