# coding: utf-8
from sympy import *
from sympy import roots, solve_poly_system
from sympy import init_printing


def main():
    # init_printing()
    init_printing(use_unicode=True)
    d_a, d_m, d_t, d_ap, d_mp, d_tp, d_total, H, A, M, T = symbols(
        "d_a d_m d_t d_ap d_mp d_tp d_total H A M T"
    )
    d_total = d_ap + d_mp + d_tp
    T = H / d_total
    T = T.subs(d_ap, d_a * 100 / (100 + A))
    T = T.subs(d_mp, d_m * 100 / (100 + M))
    T = T.subs(d_tp, d_t)
    print("Tankiness T={} in seconds".format(T))
    print("diff(T, A)={}".format(diff(T, A)))
    # dT_A = diff(T, A)


def main_old():
    global m
    init_printing()
    print("Matrix (rows are attacks, cols are defences) {}".format(m))

    nb_attacks = m.shape[0]
    nb_defs = m.shape[1]

    # Creating A0, A1, ..., Am variables. These represent the probability of doing each attack.
    asyms = create_symbols("A", nb_attacks)
    # Creating D0, D1, ..., Dn variables. These represent the probability of doing each defence.
    dsyms = create_symbols("D", nb_defs)
    print(asyms)
    print(dsyms)
    poly = 0
    # Calculating the expected value of the match symbolically.
    # e.g. Matrix([[1, 0, -1], [-1, 1, 1], [0, -1, 1]]) gives:
    # poly = A1 * (D1 - D3) + A2 * (-D1 + D2 + D3) + A3 * (-D2 + D3)
    for i in range(m.shape[0]):
        # print("Attack {} has values {}".format(i, m.row(i)))
        for j in range(m.shape[1]):
            poly += asyms[i] * dsyms[j] * m.row(i)[j]
    print("Expected value (base) = {}".format(poly))
    expanded = expand(poly)
    collected = collect(expanded, dsyms)

    print("expanded = {}".format(expanded))
    print("collected = {}".format(collected))
    coeffs = []
    for i in range(nb_defs):
        coeffs.append(collected.coeff(dsyms[i]))
    print("coeffs = {}".format(coeffs))
    equations = []
    for i in range(len(coeffs) - 1):
        # All of the defender choices should be equally impactful for this to be the optimal attack
        equations.append(coeffs[i] - coeffs[i + 1])
    last_eq = -1
    for i in range(nb_attacks):
        # The last equation is just A0+A1+...+An = 1
        last_eq += asyms[i]
    equations.append(last_eq)
    print("equations = {}".format(equations))

    # Solving the set of linear equations (this will give the optimal attack)
    results = solve(equations)
    print("results = {}".format(results))
    # If the attack is optimal, then any defences yield the same expected value. We can, for example, evaluate one of the "coeffs" with the found attack to calculate the EV:
    print(
        "Expected value under optimal play from both sides (+ means attack wins) = {}".format(
            coeffs[0].subs(results)
        )
    )


if __name__ == "__main__":
    main()


"""
Trying to work with d_a_percent and co. Can't solve for 3 variables and 3 eq ?? But with 2 variables and 2 eq, then if I subs in the 3rd equation it works? Weird. Moving on.
    d_a, d_m, d_t, d_a_percent, d_m_percent, d_t_percent = symbols(
        "d_a d_m d_t d_a_percent d_m_percent d_t_percent"
    )
    equations = []
    equations.append(d_a_percent - d_a / (d_a + d_m + d_t))
    equations.append(d_m_percent - d_m / (d_a + d_m + d_t))
    equations.append(d_t_percent - d_t / (d_a + d_m + d_t))
    # print("Eq before swap : {}".format(equations[0]))
    # equations[0] = equations[0].subs(
    #     d_a, (d_a_percent * d_t / (d_a_percent + d_m_percent - 1))
    # )
    # equations[0] = equations[0].subs(
    #     d_m, -d_m_percent * d_t / (d_a_percent + d_m_percent - 1)
    # )
    # print("Eq after swap : {}".format(equations[0]))

    results = solve(equations, [d_a, d_m, d_t], set=True)
    print("results = {}".format(results))

"""