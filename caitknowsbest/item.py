# -*- coding: utf-8 -*-
# Author : Rémi Fabre


class Item:
    def __init__(
        self,
        name,
        totalCost,
        combineCost,
        listOfSubItems=[],
        bonusAd=0,
        bonusAs=0,
        bonusOnHit=0,
        bonusCritChance=0,
        bonusBurst=0,
        bonusHp=0,
        bonusArmor=0,
        bonusMr=0,
        bonusLethality=0,
        bonusLifeSteal=0,
        isUnique=False,
        bonusHaste=0,
        percentArmorPen=0,
        isFinished=False,
    ):
        self.name = name
        self.totalCost = totalCost
        self.combineCost = combineCost
        self.listOfSubItems = listOfSubItems
        self.bonusAd = bonusAd
        self.bonusAs = bonusAs
        self.bonusOnHit = bonusOnHit
        self.bonusCritChance = bonusCritChance
        self.bonusBurst = bonusBurst
        self.bonusHp = bonusHp
        self.bonusArmor = bonusArmor
        self.bonusMr = bonusMr
        self.bonusLethality = bonusLethality
        self.bonusLifeSteal = bonusLifeSteal
        self.bonusHaste = bonusHaste
        self.percentArmorPen = percentArmorPen
        self.isFinished = isFinished

    def __repr__(self):
        # To simplify stuff
        return self.name
        output = []
        for key in self.__dict__:
            output.append("{key}='{value}'\n".format(key=key, value=self.__dict__[key]))

        return ",    *".join(output)

    def priceToCompleItem(self, inventory):
        alreadyPaid = 0
        for item in inventory:
            for neededItem in self.listOfSubItems:
                if item.name == neededItem.name:
                    alreadyPaid = alreadyPaid + item.totalCost
        return self.totalPrice - alreadypaid


def containsItem(inv, name):
    for item in inv:
        if item.name == name:
            return True
    return False
