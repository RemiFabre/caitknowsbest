# -*- coding: utf-8 -*-
# Author : Rémi Fabre


class Spell:
    def __init__(
        self,
        name,
        dmgPerLevel,
        ratioPerLevel,
        cdPerLevel,
        lvl,
        isBonusRatio=False,
        bonusAdPerLvl=[0, 0, 0, 0, 0],
    ):
        self.name = name
        self.dmgPerLevel = dmgPerLevel
        self.ratioPerLevel = ratioPerLevel
        self.cdPerLevel = cdPerLevel
        self.lvl = lvl
        self.isBonusRatio = isBonusRatio
        self.bonusAdPerLvl = bonusAdPerLvl

    def __repr__(self):
        output = []
        for key in self.__dict__:
            output.append("{key}='{value}'\n".format(key=key, value=self.__dict__[key]))
        return ",    *".join(output)

    def getDmgAndCdr(self, totalAd, bonusAd):
        if self.lvl == 0:
            # print ("Warning, asking dmg from a lvl 0 spell")
            return [0, 1]
        if self.isBonusRatio:
            dmg = (
                self.dmgPerLevel[self.lvl - 1]
                + bonusAd * self.ratioPerLevel[self.lvl - 1]
            )
        else:
            dmg = (
                self.dmgPerLevel[self.lvl - 1]
                + totalAd * self.ratioPerLevel[self.lvl - 1]
            )
        return [dmg, self.cdPerLevel[self.lvl - 1]]

    def lvlUp(self):
        self.lvl = self.lvl + 1
        if self.lvl > 5:
            raise ValueError(
                "A spell can not be of a level higher than 5. Spell : {spell}".format(
                    spell=self.name
                )
            )
