# -*- coding: utf-8 -*-
# Author : Rémi Fabre

from item import Item
import copy

AD_COST = 35
AS_COST = 25
CRIT_COST = 40
LETHALITY_COST = 29  # Open to discussion. https://leagueoflegends.fandom.com/wiki/Gold_efficiency_(League_of_Legends)
HASTE_COST = 26.67
HP_COST = 2.67
ARMOR_COST = 20
MR_COST = 18
ITEMS = {}
MANDATORY = {}
TEMP = {}

# How to fill this :
# Item(name, totalCost, combineCost, listOfSubItems=[], bonusAd=0, bonusAs=0, bonusOnHit=0, bonusCritChance=0, bonusBurst=0, bonusHp=0, bonusArmor=0, bonusMr=0, bonusLethality=0, bonusLifeSteal=0)

# Temp items to make comparisons easier
TEMP["200gOfAD"] = Item(
    "200gOfAD",
    200,
    200,
    [],
    bonusAd=5.714285714285714,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
TEMP["300gOfAD"] = Item(
    "300gOfAD",
    300,
    300,
    [],
    bonusAd=5.714285714285714 * 1.5,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
TEMP["500gOfAD"] = Item(
    "500gOfAD",
    500,
    500,
    [],
    bonusAd=5.714285714285714 * 2.5,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
TEMP["200gOfAS"] = Item(
    "200gOfAS",
    200,
    200,
    [],
    bonusAd=0.0,
    bonusAs=0.08,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
TEMP["40Lethality"] = Item(
    "40Lethality",
    2900,
    2900,
    [],
    bonusAd=0.0,
    bonusAs=0.00,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=40,
)
TEMP["800gOfHP"] = Item(
    "800gOfHP",
    800,
    800,
    [],
    bonusAd=0.0,
    bonusAs=0.00,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=300,
    bonusArmor=0,
    bonusMr=0,
)
TEMP["100Haste"] = Item(
    "100Haste",
    10000,
    10000,
    [],
    bonusAd=0.0,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusHaste=100,
    bonusMr=0,
)

# The mandatory section is used to list items that can be forced into the build (e.g. boots) but shouldn't add complexity when brute forcing the search => Not used anymore
# TODO item changed in S11 and isn't high prio anymore
# ITEMS["tiamat"] = Item(
#     "tiamat",
#     1325,
#     625,
#     ["sword", "sword"],
#     bonusAd=25,
#     bonusAs=0.0,
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
#     bonusCdr=0.0,
#     isUnique=True,
# )

ITEMS["boots"] = Item(
    "boots",
    300,
    300,
    [""],
    bonusAd=0,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=0,
    bonusHaste=0.0,
    isUnique=True,
)

ITEMS["ionanBoots"] = Item(
    "ionanBoots",
    900,
    900,
    [""],
    bonusAd=0,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=0,
    bonusHaste=15,
    isUnique=True,
)

ITEMS["berserkerBoots"] = Item(
    "berserkerBoots",
    1100,
    1100,
    [],
    bonusAd=0,
    bonusAs=0.35,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=0,
    bonusHaste=0.0,
    isUnique=True,
)

# Items
ITEMS["sword"] = Item(
    "sword",
    350,
    350,
    [],
    bonusAd=10,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
ITEMS["pickAxe"] = Item(
    "pickAxe",
    875,
    875,
    [],
    bonusAd=25,
    bonusAs=0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
ITEMS["BF"] = Item(
    "BF",
    1300,
    1300,
    [],
    bonusAd=40,
    bonusAs=0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
ITEMS["noonquiver"] = Item(
    "noonquiver",
    1300,
    1300,
    [],
    bonusAd=30,
    bonusAs=0.15,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
ITEMS["dirk"] = Item(
    "dirk",
    1000,
    300,
    ["sword", "sword"],
    bonusAd=20,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=10,
    isUnique=True,
)
ITEMS["warhammer"] = Item(
    "warhammer",
    1100,
    400,
    ["sword", "sword"],
    bonusAd=20,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=0,
    bonusHaste=10,
    isUnique=True,
)

# ITEMS["glove"] = Item(
#     "glove",
#     400,
#     400,
#     [],
#     bonusAd=0,
#     bonusAs=0,
#     bonusOnHit=0,
#     bonusCritChance=0.1,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
# )
ITEMS["cloak"] = Item(
    "cloak",
    600,
    600,
    [],
    bonusAd=0,
    bonusAs=0,
    bonusOnHit=0,
    bonusCritChance=0.15,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
ITEMS["dagger"] = Item(
    "dagger",
    300,
    300,
    [],
    bonusAd=0,
    bonusAs=0.10,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
# Just to have a point of comparison with the sword
ITEMS["superdagger"] = Item(
    "superdagger",
    350,
    300,
    [],
    bonusAd=0,
    bonusAs=0.1167,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)
ITEMS["recurveBow"] = Item(
    "recurveBow",
    1000,
    400,
    ["dagger", "dagger"],
    bonusAd=0,
    bonusAs=0.25,
    bonusOnHit=15,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    isUnique=True,
)

# Description up to date, but taking it out because it's not relevant for our tests for now
# ITEMS["kircheisShard"] = Item(
#     "kircheisShard",
#     700,
#     400,
#     ["dagger"],
#     bonusAd=0,
#     bonusAs=0.15,
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
# )


ITEMS["zeal"] = Item(
    "zeal",
    1100,
    200,
    ["dagger", "cloak"],
    bonusAd=0,
    bonusAs=0.15,
    bonusOnHit=0,
    bonusCritChance=0.15,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
)


ITEMS["IE"] = Item(
    "IE",
    3400,
    625,
    ["BF", "pickAxe", "cloak"],
    bonusAd=80,
    bonusAs=0,
    bonusOnHit=0,
    bonusCritChance=0.25,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    isFinished=True,
)

# TODO
# ITEMS["trinityForce"] = Item(
#     "trinityForce",
#     3733,
#     333 + 1050 + 1250,
#     ["stinger"],
#     bonusAd=25,
#     bonusAs=0.4,
#     bonusOnHit=0,
#     bonusCritChance=0.0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusCdr=0.2,
# )

ITEMS["phantomDancer"] = Item(
    "phantomDancer",
    2600,
    900,
    ["zeal", "dagger", "dagger"],
    bonusAd=20,
    bonusAs=0.6,
    bonusOnHit=0,
    bonusCritChance=0.25,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    isFinished=True,
)

ITEMS["youmuu"] = Item(
    "youmuu",
    2700,
    800,
    ["dirk", "warhammer"],
    bonusAd=60,
    bonusAs=0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=18,
    bonusHaste=0,
    isFinished=True,
)

ITEMS["collector"] = Item(
    "collector",
    3000,
    425,
    ["dirk", "pickAxe", "cloak"],
    bonusAd=60,
    bonusAs=0,
    bonusOnHit=0,
    bonusCritChance=25,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=15,
    bonusHaste=0,
    isFinished=True,
)

# Removed
# ITEMS["claw"] = Item(
#     "claw",
#     3100,
#     900,
#     ["dirk", "warhammer"],
#     bonusAd=60,
#     bonusAs=0,
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=18,
#     bonusHaste=20,
#     isFinished=True,
# )

# Now this is Navori Flickerblades
ITEMS["navori"] = Item(
    "navori",
    2600,
    900,
    ["zeal", "dagger", "dagger"],
    bonusAd=0,
    bonusAs=40,
    bonusOnHit=0,
    bonusCritChance=0.25,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusHaste=0,
    isUnique=True,
    isFinished=True,
)

# ITEMS["immortal"] = Item(
#     "immortal",
#     3400,
#     600 + 1300 + 900,
#     ["cloak"],
#     bonusAd=50,
#     bonusAs=0.15,
#     bonusOnHit=0,
#     bonusCritChance=0.20,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusHaste=0.0,
#     isUnique=True,
#     isFinished=True,
# )

# ITEMS["essenceReaver"] = Item(
#     "essenceReaver",
#     2900,
#     500 + 700,
#     ["warhammer", "cloak"],
#     bonusAd=50,
#     bonusAs=0,
#     bonusOnHit=0,
#     bonusCritChance=0.2,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusHaste=20,
#     isUnique=True,
# )

ITEMS["stridebreaker"] = Item(
    "stridebreaker",
    3300,
    3300,
    [],
    bonusAd=50,
    bonusAs=0.3,
    bonusOnHit=0,
    bonusCritChance=0.0,
    bonusBurst=0,
    bonusHp=450,
    bonusArmor=0,
    bonusMr=0,
    bonusHaste=20,
    isUnique=True,
)

# ITEMS["hullbreaker"] = Item(
#     "hullbreaker",
#     2800,
#     2800,
#     [],
#     bonusAd=50,
#     bonusAs=0.0,
#     bonusOnHit=0,
#     bonusCritChance=0.0,
#     bonusBurst=0,
#     bonusHp=400,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusHaste=0.0,
#     isUnique=True,
# )

ITEMS["hydra"] = Item(
    "hydra",
    3300,
    2100,
    ["tiamat"],
    bonusAd=70,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=0,
    bonusHaste=20,
    isUnique=True,
)

ITEMS["profane"] = Item(
    "profane",
    3300,
    2100,
    ["tiamat"],
    bonusAd=60,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=18,
    bonusHaste=20,
    isUnique=True,
)

# This item is now removed.
# This item bonus AS is handled in the champion update because it depends on the lvl
# ITEMS["sanguineBlade"] = Item(
#     "sanguineBlade",
#     3000,
#     1900,
#     ["dirk"],
#     bonusAd=50,
#     bonusAs=0.0,
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=18,
#     isUnique=True,
# )

# Fictive item created to give a chance to LS items in pure dmg comparisons.
ITEMS["superBF"] = Item(
    "superBF",
    2800,
    0,
    ["BF", "BF"],
    bonusAd=80,
    bonusAs=0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=0,
)

# To be updated
# ITEMS["chargedGuinsoo"] = Item(
#     "chargedGuinsoo",
#     2800,
#     1100,
#     ["rageknife", "cloak", "dagger"],
#     bonusAd=0,
#     bonusAs=0.40,
#     bonusOnHit=0,
#     bonusCritChance=0.2,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
#     percentArmorPen=0.0,
#     isFinished=True,
# )

# Removed
# ITEMS["rageknife"] = Item(
#     "rageknife",
#     800,
#     200,
#     ["dagger", "dagger"],
#     bonusAd=0,
#     bonusAs=0.25,
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
#     percentArmorPen=0.0,
# )

# To be updated
# ITEMS["kraken"] = Item(
#     "kraken",
#     3400,
#     1300 + 625,
#     ["pickaxe", "cloak"],
#     bonusAd=65,
#     bonusAs=0.25,
#     bonusOnHit=0,
#     bonusCritChance=0.2,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
#     percentArmorPen=0.0,
# )

# Removed :'(
# ITEMS["galeForce"] = Item(
#     "galeForce",
#     3400,
#     1300 + 625,
#     ["pickaxe", "cloak"],
#     bonusAd=60,
#     bonusAs=0.20,
#     bonusOnHit=0,
#     bonusCritChance=0.2,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
#     percentArmorPen=0.0,
# )


ITEMS["lastWhisper"] = Item(
    "lastWhisper",
    1450,
    750,
    ["sword", "sword"],
    bonusAd=20,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=0,
    percentArmorPen=0.18,
    isUnique=True,
)

ITEMS["lordDominik"] = Item(
    "lordDominik",
    3000,
    950,
    ["lastWhisper", "cloak"],
    bonusAd=45,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0.25,
    bonusBurst=0,
    bonusHp=0,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=0,
    percentArmorPen=0.4,
    isUnique=True,
    isFinished=True,
)

# To be updated
# ITEMS["serylda"] = Item(
#     "serylda",
#     3200,
#     650,
#     ["lastWhisper", "warhammer"],
#     bonusAd=45,
#     bonusAs=0.0,
#     bonusOnHit=0,
#     bonusCritChance=0.0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
#     percentArmorPen=0.3,
#     bonusHaste=20,
#     isUnique=True,
#     isFinished=True,
# )

# Removed
# ITEMS["goredrinker"] = Item(
#     "goredrinker",
#     3300,
#     3300,
#     [],
#     bonusAd=45,
#     bonusAs=0.0,
#     bonusOnHit=0,
#     bonusCritChance=0.0,
#     bonusBurst=0,
#     bonusHp=400,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
#     percentArmorPen=0.0,
#     bonusHaste=20,
#     isUnique=True,
#     isFinished=False,
# )

ITEMS["titanic"] = Item(
    "titanic",
    3300,
    3300,
    [],
    bonusAd=30,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0.0,
    bonusBurst=0,
    bonusHp=500,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=0,
    percentArmorPen=0.0,
    bonusHaste=0,
    isUnique=True,
    isFinished=True,
)

ITEMS["shojin"] = Item(
    "shojin",
    3400,
    3400,
    [],
    bonusAd=65,
    bonusAs=0.0,
    bonusOnHit=0,
    bonusCritChance=0.0,
    bonusBurst=0,
    bonusHp=300,
    bonusArmor=0,
    bonusMr=0,
    bonusLethality=0,
    percentArmorPen=0.0,
    bonusHaste=20,
    isUnique=True,
    isFinished=True,
)

# Adding items twice so they can be bought twice... Don't judge me... TODO do better than this
ITEMS["sword_"] = copy.deepcopy(ITEMS["sword"])
ITEMS["pickAxe_"] = copy.deepcopy(ITEMS["pickAxe"])
ITEMS["BF_"] = copy.deepcopy(ITEMS["BF"])
ITEMS["cloak_"] = copy.deepcopy(ITEMS["cloak"])
ITEMS["dagger_"] = copy.deepcopy(ITEMS["dagger"])
ITEMS["zeal_"] = copy.deepcopy(ITEMS["zeal"])
ITEMS["superBF_"] = copy.deepcopy(ITEMS["superBF"])


# Old Runes keeping them for the nostalgia
# MARKS["lethalityMarks"] = Item(
#     "lethalityMarks",
#     0,
#     0,
#     [],
#     bonusAd=0,
#     bonusAs=0,
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=(1.6 * 9),
# )
# MARKS["adMarks"] = Item(
#     "adMarks",
#     0,
#     0,
#     [],
#     bonusAd=(0.95 * 9),
#     bonusAs=0,
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
# )
# MARKS["asMarks"] = Item(
#     "asMarks",
#     0,
#     0,
#     [],
#     bonusAd=0,
#     bonusAs=(0.017 * 9),
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
# )
# MARKS["critMarks"] = Item(
#     "critMarks",
#     0,
#     0,
#     [],
#     bonusAd=0,
#     bonusAs=0,
#     bonusOnHit=0,
#     bonusCritChance=(9 * 0.0093),
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
# )

# QUINTS["adQuints"] = Item(
#     "adQuints",
#     0,
#     0,
#     [],
#     bonusAd=(2.25 * 3),
#     bonusAs=0,
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
# )
# QUINTS["lethalityQuints"] = Item(
#     "lethalityQuints",
#     0,
#     0,
#     [],
#     bonusAd=0,
#     bonusAs=0,
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=3.2 * 3,
# )
# QUINTS["asQuints"] = Item(
#     "asQuints",
#     0,
#     0,
#     [],
#     bonusAd=0,
#     bonusAs=(0.045 * 3),
#     bonusOnHit=0,
#     bonusCritChance=0,
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
# )
# QUINTS["critQuints"] = Item(
#     "critQuints",
#     0,
#     0,
#     [],
#     bonusAd=0,
#     bonusAs=0,
#     bonusOnHit=0,
#     bonusCritChance=(3 * 0.0186),
#     bonusBurst=0,
#     bonusHp=0,
#     bonusArmor=0,
#     bonusMr=0,
#     bonusLethality=0,
# )
