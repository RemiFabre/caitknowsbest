import argparse
from argparse import RawTextHelpFormatter


from champ import *
from spell import *
from item import *
from items import *

from tryndamere_optimization import (
    tryndamereItemOptimization,
    tryndamereCalculation,
    tryndamereCompareInventories,
)

from utils import InputDps


def tryndamere_inventory_tests(args):
    listOfInventories = []

    # Pre ER
    # listOfInventories.append([ITEMS["tiamat"], ITEMS["BF"], ITEMS["dagger"]])
    # listOfInventories.append([ITEMS["tiamat"], ITEMS["cloak"], ITEMS["cloak"]])
    # listOfInventories.append([ITEMS["tiamat"], ITEMS["berserkerBoots"], ITEMS["sword"]])
    # listOfInventories.append(
    #     [ITEMS["tiamat"], ITEMS["berserkerBoots"], ITEMS["sword"], ITEMS["sword"]]
    # )
    # listOfInventories.append([ITEMS["tiamat"], ITEMS["warhammer"], ITEMS["dagger"]])
    # listOfInventories.append(
    #     [ITEMS["tiamat"], ITEMS["warhammer"], ITEMS["dagger"], ITEMS["dagger"]]
    # )

    # AD shard + ionan with delayed ER to get some AS?
    # listOfInventories.append(
    #     [
    #         ITEMS["tiamat"],
    #         ITEMS["ionanBoots"],
    #         ITEMS["BF"],
    #         ITEMS["recurveBow"],
    #         ITEMS["cloak"],
    #         ITEMS["sword"],
    #     ]
    # )

    # CDR boots vs CDR runes late (lvl 15)
    # listOfInventories.append(
    #     [
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["chargedGuinsoo"],
    #         ITEMS["ionanBoots"],
    #         TEMP["200gOfAD"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["chargedGuinsoo"],
    #         ITEMS["berserkerBoots"],
    #     ]
    # )

    # CDR boots vs CDR runes mid (lvl 12)
    # listOfInventories.append(
    #     [
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["ionanBoots"],
    #         TEMP["200gOfAS"],
    #     ]
    # )
    # listOfInventories.append(
    #     [ITEMS["tiamat"], ITEMS["essenceReaver"], ITEMS["berserkerBoots"],]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["boots"],
    #         ITEMS["dagger"],
    #         ITEMS["dagger"],
    #     ]
    # )

    # CDR boots vs CDR runes early (lvl 6)
    # listOfInventories.append(
    #     [ITEMS["tiamat"],]
    # )

    # When to buy second cloak ? with Ioanan path (lvl 12)
    # listOfInventories.append(
    #     [
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["boots"],
    #         ITEMS["cloak"],
    #         TEMP["200gOfAS"],
    #     ]
    # )

    # lvl 15 guinsoo spike vs old build
    # listOfInventories.append(
    #     [
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["ionanBoots"],
    #         ITEMS["cloak"],
    #         ITEMS["chargedGuinsoo"],
    #         TEMP["500gOfAD"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["cloak"],
    #         ITEMS["IE"],
    #     ]
    # )
    # listOfInventories.append(
    #     [ITEMS["tiamat"], ITEMS["essenceReaver"], ITEMS["boots"], ITEMS["recurveBow"],]
    # )

    # listOfInventories.append(
    #     [ITEMS["sword"],]
    # )
    # Hydra first tests late
    # listOfInventories.append(
    #     [
    #         ITEMS["hydra"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["cloak"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["hydra"],
    #         ITEMS["stinger"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["cloak"],
    #         ITEMS["cloak"],
    #         ITEMS["BF"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["cloak"],
    #         ITEMS["stinger"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["hydra"],
    #         ITEMS["statikkShiv"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["cloak"],
    #         ITEMS["stinger"],
    #     ]
    # )
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["trinityForce"], ITEMS["berserkerBoots"],]
    # )
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["trinityForce"], ITEMS["berserkerBoots"], ITEMS["cloak"]]
    # )
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["IE"], ITEMS["berserkerBoots"], ITEMS["cloak"]]
    # )
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["IE"], ITEMS["berserkerBoots"], ITEMS["stinger"]]
    # )

    # # Hydra first tests early
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["berserkerBoots"],]
    # )
    # # After Hydra stinger ~ cloak >> rest
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["stinger"], ITEMS["berserkerBoots"],]
    # )
    # listOfInventories.append([ITEMS["hydra"], ITEMS["berserkerBoots"], ITEMS["cloak"]])
    # listOfInventories.append([ITEMS["hydra"], ITEMS["berserkerBoots"], ITEMS["BF"]])
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["berserkerBoots"], ITEMS["warhammer"]]
    # )

    # # After Hydra + stinger
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["stinger"], ITEMS["berserkerBoots"], ITEMS["warhammer"]]
    # )
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["stinger"], ITEMS["berserkerBoots"], ITEMS["cloak"]]
    # )
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["stinger"], ITEMS["berserkerBoots"], ITEMS["BF"]]
    # )
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["essenceReaver"], ITEMS["berserkerBoots"]]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["hydra"],
    #         ITEMS["stinger"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["cloak"],
    #         ITEMS["warhammer"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["hydra"],
    #         ITEMS["stinger"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["cloak"],
    #         ITEMS["cloak"],
    #     ]
    # )

    # # Hydra vs ER
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["berserkerBoots"], ITEMS["stinger"]]
    # )
    # listOfInventories.append(
    #     [ITEMS["essenceReaver"], ITEMS["berserkerBoots"], ITEMS["tiamat"]]
    # )
    # listOfInventories.append(
    #     [ITEMS["hydra"], ITEMS["stinger"], ITEMS["berserkerBoots"],]
    # )
    # listOfInventories.append(
    #     [ITEMS["essenceReaver"], ITEMS["stinger"], ITEMS["berserkerBoots"],]
    # )

    # for lvl in range(8, 13):
    #     # Sanguine first ? => Nope
    #     listOfInventories.append(
    #         [ITEMS["essenceReaver"], ITEMS["berserkerBoots"],]
    #     )
    #     listOfInventories.append(
    #         [ITEMS["sanguineBlade"], ITEMS["berserkerBoots"], TEMP["300gOfAD"]]
    #     )

    #     tryndamereCompareInventories(lvl, listOfInventories, dpsVersion=True)

    # # statikkShiv rush ?
    # # lvl 9
    # listOfInventories.append([ITEMS["statikkShiv"]])
    # listOfInventories.append([ITEMS["berserkerBoots"], ITEMS["tiamat"]])

    # listOfInventories.append([ITEMS["statikkShiv"], ITEMS["berserkerBoots"]])
    # listOfInventories.append([ITEMS["berserkerBoots"], ITEMS["tiamat"], ITEMS["BF"]])
    # tryndamereCompareInventories(9, listOfInventories, dpsVersion=True)

    # # lvl 13
    # listOfInventories = []
    # listOfInventories.append(
    #     [
    #         ITEMS["statikkShiv"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["dagger"],
    #         ITEMS["dagger"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["berserkerBoots"],
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["cloak"],
    #         ITEMS["stinger"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["statikkShiv"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["IE"],
    #         ITEMS["dagger"],
    #         ITEMS["dagger"],
    #     ]
    # )
    # tryndamereCompareInventories(13, listOfInventories, dpsVersion=True)

    # # lvl 14
    # listOfInventories = []
    # listOfInventories.append(
    #     [
    #         ITEMS["statikkShiv"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["stinger"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["berserkerBoots"],
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["cloak"],
    #         ITEMS["stinger"],
    #         TEMP["500gOfAD"],
    #     ]
    # )
    # listOfInventories.append(
    #     [ITEMS["statikkShiv"], ITEMS["berserkerBoots"], ITEMS["IE"], ITEMS["stinger"],]
    # )

    # tryndamereCompareInventories(14, listOfInventories, dpsVersion=True)

    # # lvl 16
    # listOfInventories = []
    # listOfInventories.append(
    #     [
    #         ITEMS["statikkShiv"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["trinityForce"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["berserkerBoots"],
    #         ITEMS["tiamat"],
    #         ITEMS["essenceReaver"],
    #         ITEMS["IE"],
    #         ITEMS["stinger"],
    #     ]
    # )
    # listOfInventories.append(
    #     [
    #         ITEMS["statikkShiv"],
    #         ITEMS["berserkerBoots"],
    #         ITEMS["IE"],
    #         ITEMS["trinityForce"],
    #     ]
    # )

    # tryndamereCompareInventories(16, listOfInventories, dpsVersion=True)

    # tryndamereCompareInventories(args.lvl, listOfInventories, dpsVersion=True)

    # HoB testing
    listOfInventories = []
    # listOfInventories.append([ITEMS["tiamat"], ITEMS["berserkerBoots"], ITEMS["sword"]])
    # tryndamereCompareInventories(18, listOfInventories, hobMode=True)

    # listOfInventories = []
    # listOfInventories.append([ITEMS["sword"], ITEMS["sword"], ITEMS["sword"]])
    # listOfInventories.append([ITEMS["pickAxe"]])
    # listOfInventories.append([ITEMS["rageknife"]])
    # listOfInventories.append([ITEMS["BF"]])
    # listOfInventories.append([ITEMS["dirk"]])
    # tryndamereCompareInventories(
    #     6,
    #     listOfInventories,
    #     hobMode=True,
    #     dpsVersion=False,
    #     rune="HoB",
    #     minorRune1="AS",
    #     minorRune2="AD",
    #     hasTranscendence=False,
    #     hasCosmic=False,
    #     currentPercentHP=0.5,
    # )
    # sys.exit()

    ##### early lvls, is AS better than AD ? No, unless no lethal tempo.
    # listOfInventories = []
    # listOfInventories.append([ITEMS["superdagger"]])
    # listOfInventories.append(
    #     [
    #         ITEMS["sword"],
    #     ]
    # )
    # tryndamereCompareInventories(
    #     1,
    #     listOfInventories,
    #     hobMode=args.hob,
    #     dpsVersion=True,
    #     rune="None",
    #     minorRune1="AS",
    #     minorRune2="AD",
    #     hasTranscendence=False,
    #     hasCosmic=False,
    #     currentPercentHP=args.hp,
    # )
    # tryndamereCompareInventories(
    #     2,
    #     listOfInventories,
    #     hobMode=args.hob,
    #     dpsVersion=True,
    #     rune="None",
    #     minorRune1="AS",
    #     minorRune2="AD",
    #     hasTranscendence=False,
    #     hasCosmic=False,
    #     currentPercentHP=args.hp,
    # )
    # tryndamereCompareInventories(
    #     1,
    #     listOfInventories,
    #     hobMode=args.hob,
    #     dpsVersion=True,
    #     rune="Tempo",
    #     minorRune1="AS",
    #     minorRune2="AD",
    #     hasTranscendence=False,
    #     hasCosmic=False,
    #     currentPercentHP=args.hp,
    # )
    # tryndamereCompareInventories(
    #     2,
    #     listOfInventories,
    #     hobMode=args.hob,
    #     dpsVersion=True,
    #     rune="Tempo",
    #     minorRune1="AS",
    #     minorRune2="AD",
    #     hasTranscendence=False,
    #     hasCosmic=False,
    #     currentPercentHP=args.hp,
    # )
    # print("Done !")

    listOfInventories = []
    listOfInventories.append([ITEMS["hydra"]])
    listOfInventories.append([ITEMS["kraken"]])
    listOfInventories.append([ITEMS["galeForce"]])
    listOfInventories.append([ITEMS["stridebreaker"]])
    listOfInventories.append([ITEMS["goredrinker"]])
    listOfInventories.append([ITEMS["navori"]])
    listOfInventories.append([ITEMS["IE"]])
    listOfInventories.append([ITEMS["phantomDancer"], ITEMS["sword"], ITEMS["sword"]])
    listOfInventories.append([ITEMS["claw"]])
    listOfInventories.append([ITEMS["zeal"]])
    listOfInventories.append([ITEMS["sword"], ITEMS["sword"], ITEMS["sword"]])
    tryndamereCompareInventories(
        9,
        listOfInventories,
        hobMode=args.hob,
        dpsVersion=True,
        rune="Tempo",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=args.hp,
    )

    listOfInventories = []
    listOfInventories.append([ITEMS["phantomDancer"], ITEMS["kraken"]])
    listOfInventories.append([ITEMS["phantomDancer"], ITEMS["hydra"]])
    listOfInventories.append([ITEMS["phantomDancer"], ITEMS["galeForce"]])
    listOfInventories.append([ITEMS["phantomDancer"], ITEMS["IE"]])
    listOfInventories.append([ITEMS["phantomDancer"], ITEMS["navori"]])
    listOfInventories.append(
        [ITEMS["phantomDancer"], ITEMS["phantomDancer"], ITEMS["sword"], ITEMS["sword"]]
    )
    listOfInventories.append(
        [ITEMS["phantomDancer"], ITEMS["lordDominik"], ITEMS["sword"]]
    )
    listOfInventories.append([ITEMS["phantomDancer"], ITEMS["stridebreaker"]])
    listOfInventories.append(
        [
            ITEMS["phantomDancer"],
            ITEMS["hullbreaker"],
            TEMP["300gOfAD"],
            TEMP["300gOfAD"],
        ]
    )

    tryndamereCompareInventories(
        13,
        listOfInventories,
        hobMode=args.hob,
        dpsVersion=True,
        rune="Tempo",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=args.hp,
    )
    listOfInventories = []

    listOfInventories.append(
        [ITEMS["phantomDancer"], ITEMS["hullbreaker"], ITEMS["kraken"]]
    )
    listOfInventories.append(
        [ITEMS["phantomDancer"], ITEMS["hullbreaker"], ITEMS["galeForce"]]
    )
    listOfInventories.append(
        [ITEMS["phantomDancer"], ITEMS["hullbreaker"], ITEMS["IE"]]
    )
    listOfInventories.append(
        [ITEMS["phantomDancer"], ITEMS["hullbreaker"], ITEMS["navori"]]
    )
    listOfInventories.append(
        [ITEMS["phantomDancer"], ITEMS["hullbreaker"], ITEMS["claw"]]
    )
    listOfInventories.append(
        [
            ITEMS["phantomDancer"],
            ITEMS["hullbreaker"],
            ITEMS["lordDominik"],
            ITEMS["sword"],
        ]
    )

    tryndamereCompareInventories(
        15,
        listOfInventories,
        hobMode=args.hob,
        dpsVersion=True,
        rune="Tempo",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=args.hp,
    )

    listOfInventories = []

    listOfInventories.append(
        [
            ITEMS["phantomDancer"],
            ITEMS["kraken"],
            ITEMS["phantomDancer"],
            TEMP["500gOfAD"],
            TEMP["300gOfAD"],
        ]
    )
    listOfInventories.append(
        [
            ITEMS["phantomDancer"],
            ITEMS["kraken"],
            ITEMS["hullbreaker"],
            TEMP["300gOfAD"],
            TEMP["300gOfAD"],
        ]
    )
    listOfInventories.append([ITEMS["phantomDancer"], ITEMS["kraken"], ITEMS["IE"]])
    listOfInventories.append([ITEMS["phantomDancer"], ITEMS["kraken"], ITEMS["navori"]])
    listOfInventories.append(
        [ITEMS["phantomDancer"], ITEMS["kraken"], ITEMS["claw"], TEMP["300gOfAD"]]
    )
    listOfInventories.append(
        [ITEMS["phantomDancer"], ITEMS["kraken"], ITEMS["lordDominik"], ITEMS["sword"]]
    )

    tryndamereCompareInventories(
        15,
        listOfInventories,
        hobMode=args.hob,
        dpsVersion=True,
        rune="Tempo",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=args.hp,
    )


def tryndamere_hydra_exporation():
    # LVL 9 ~3400 gold
    listOfInventories = []
    listOfInventories.append(
        [ITEMS["phantomDancer"], TEMP["500gOfAD"], TEMP["300gOfAD"]]
    )
    listOfInventories.append([ITEMS["hydra"]])
    listOfInventories.append([ITEMS["kraken"]])
    listOfInventories.append([ITEMS["stridebreaker"]])
    listOfInventories.append([ITEMS["IE"]])
    listOfInventories.append([ITEMS["navori"]])
    listOfInventories.append([ITEMS["claw"]])
    # Sorting by real damage
    tryndamereCompareInventories(
        9,
        listOfInventories,
        hobMode=False,
        dpsVersion=True,
        rune="None",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=0.5,
        ultimate_on=False,
        inputDps=InputDps(25, 100, 100),
        sortIndexes=[6],
    )

    # LVL 13 ~ 6800 gold
    listOfInventories = []
    listOfInventories.append([ITEMS["hydra"], ITEMS["kraken"]])
    listOfInventories.append([ITEMS["hydra"], ITEMS["phantomDancer"], TEMP["800gOfHP"]])
    listOfInventories.append([ITEMS["hydra"], ITEMS["galeForce"]])
    listOfInventories.append([ITEMS["hydra"], ITEMS["IE"]])
    listOfInventories.append([ITEMS["hydra"], ITEMS["navori"]])
    listOfInventories.append(
        [
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            TEMP["200gOfAS"],
            TEMP["200gOfAS"],
            TEMP["200gOfAS"],
        ]
    )
    listOfInventories.append([ITEMS["hydra"], ITEMS["lordDominik"], ITEMS["sword"]])
    listOfInventories.append([ITEMS["hydra"], ITEMS["stridebreaker"]])
    # Sorting by real damage
    tryndamereCompareInventories(
        13,
        listOfInventories,
        hobMode=False,
        dpsVersion=True,
        rune="None",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=0.5,
        ultimate_on=False,
        inputDps=InputDps(25, 100, 100),
        sortIndexes=[6],
    )

    # LVL 15 ~ 9500 gold
    listOfInventories = []
    listOfInventories.append([ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["dirk"]])
    listOfInventories.append([ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["claw"]])
    listOfInventories.append([ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"]])
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"], ITEMS["dirk"]]
    )
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["phantomDancer"], ITEMS["cloak"]]
    )
    listOfInventories.append([ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["kraken"]])
    listOfInventories.append([ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["IE"]])
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["lordDominik"]]
    )
    # Sorting by real damage
    tryndamereCompareInventories(
        15,
        listOfInventories,
        hobMode=False,
        dpsVersion=True,
        rune="Tempo",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=0.5,
        ultimate_on=False,
        inputDps=InputDps(25, 100, 100),
        sortIndexes=[6],
    )

    # LVL 16 ~ 11000+ gold
    listOfInventories = []

    listOfInventories.append(
        [
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            ITEMS["phantomDancer"],
            ITEMS["cloak"],
            ITEMS["BF"],
        ]
    )
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"], ITEMS["kraken"]]
    )
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"], ITEMS["galeForce"]]
    )
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"], ITEMS["IE"]]
    )
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"], ITEMS["lordDominik"]]
    )
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"], ITEMS["claw"]]
    )
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"], ITEMS["navori"]]
    )
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"], TEMP["40Lethality"]]
    )
    listOfInventories.append(
        [ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"], ITEMS["hullbreaker"]]
    )
    listOfInventories.append([ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"]])

    # Sorting by real damage
    tryndamereCompareInventories(
        16,
        listOfInventories,
        hobMode=False,
        dpsVersion=True,
        rune="Tempo",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=0.5,
        ultimate_on=False,
        inputDps=InputDps(25, 100, 100),
        sortIndexes=[6],
    )

    # Default 4 items
    listOfInventories = []

    listOfInventories.append(
        [
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            ITEMS["phantomDancer"],
            ITEMS["galeForce"],
            ITEMS["youmuu"],
        ]
    )
    listOfInventories.append(
        [
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            ITEMS["phantomDancer"],
            ITEMS["galeForce"],
            ITEMS["collector"],
        ]
    )
    listOfInventories.append(
        [
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            ITEMS["phantomDancer"],
            ITEMS["galeForce"],
            ITEMS["IE"],
        ]
    )
    listOfInventories.append(
        [
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            ITEMS["phantomDancer"],
            ITEMS["galeForce"],
            ITEMS["navori"],
        ]
    )
    listOfInventories.append(
        [
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            ITEMS["phantomDancer"],
            ITEMS["galeForce"],
            ITEMS["serylda"],
        ]
    )
    listOfInventories.append(
        [
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            ITEMS["phantomDancer"],
            ITEMS["galeForce"],
            ITEMS["lordDominik"],
        ]
    )

    listOfInventories.append(
        [
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            ITEMS["phantomDancer"],
            ITEMS["galeForce"],
            ITEMS["titanic"],
        ]
    )
    listOfInventories.append(
        [
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            ITEMS["phantomDancer"],
            ITEMS["galeForce"],
            ITEMS["shojin"],
        ]
    )

    # Sorting by real damage
    tryndamereCompareInventories(
        16,
        listOfInventories,
        hobMode=False,
        dpsVersion=True,
        rune="Tempo",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=0.5,
        ultimate_on=False,
        inputDps=InputDps(25, 100, 100),
        sortIndexes=[6],
    )


# path 14.10
# Build seems to be profane -> IE -> (probably lord dominiks) -> (what is needed as non crit, I like BT, edge of night, Youmuu, Hexplate)
def tryndamere_hob_exporation():
    print("")
    print("############### LVL 9, 1 item")
    print("")
    # LVL 9 ~3400 gold
    listOfInventories = []
    listOfInventories.append(
        [ITEMS["phantomDancer"], TEMP["500gOfAD"], TEMP["300gOfAD"]]
    )
    listOfInventories.append([ITEMS["hydra"]])
    listOfInventories.append([ITEMS["profane"]])
    listOfInventories.append([ITEMS["stridebreaker"]])
    listOfInventories.append([ITEMS["IE"]])
    listOfInventories.append([ITEMS["navori"]])
    # Sorting by real damage
    tryndamereCompareInventories(
        9,
        listOfInventories,
        hobMode=True,
        dpsVersion=False,
        rune="HoB",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=0.5,
        ultimate_on=True,
        inputDps=InputDps(25, 100, 100),
        sortIndexes=[6],
    )

    print("")
    print("############### LVL 13, 2 items")
    print("")
    # LVL 13 ~ 6800 gold
    listOfInventories = []
    listOfInventories.append(
        [ITEMS["phantomDancer"], TEMP["500gOfAD"], TEMP["300gOfAD"]]
    )
    listOfInventories.append(
        [ITEMS["profane"], ITEMS["phantomDancer"], TEMP["500gOfAD"], TEMP["300gOfAD"]]
    )
    listOfInventories.append(
        [ITEMS["profane"], ITEMS["navori"], TEMP["500gOfAD"], TEMP["300gOfAD"]]
    )
    listOfInventories.append(
        [ITEMS["profane"], ITEMS["youmuu"], TEMP["300gOfAD"], TEMP["300gOfAD"]]
    )
    listOfInventories.append([ITEMS["profane"], ITEMS["lordDominik"], TEMP["300gOfAD"]])
    listOfInventories.append([ITEMS["profane"], ITEMS["IE"]])
    listOfInventories.append([ITEMS["IE"], ITEMS["lordDominik"], TEMP["300gOfAD"]])
    listOfInventories.append(
        [ITEMS["IE"], ITEMS["navori"], TEMP["500gOfAD"], TEMP["300gOfAD"]]
    )

    tryndamereCompareInventories(
        13,
        listOfInventories,
        hobMode=True,
        dpsVersion=False,
        rune="HoB",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=0.5,
        ultimate_on=True,
        inputDps=InputDps(25, 100, 100),
        sortIndexes=[6],
    )

    print("")
    print("############### LVL 15, 3 items")
    print("")

    # LVL 15 ~ 9500 gold
    listOfInventories.append(
        [
            ITEMS["IE"],
            ITEMS["lordDominik"],
            ITEMS["superBF"],
        ]
    )

    listOfInventories.append([ITEMS["profane"], ITEMS["IE"], ITEMS["lordDominik"]])
    listOfInventories.append([ITEMS["profane"], ITEMS["IE"], ITEMS["youmuu"]])
    listOfInventories.append(
        [ITEMS["profane"], ITEMS["IE"], ITEMS["navori"], TEMP["500gOfAD"]]
    )

    tryndamereCompareInventories(
        15,
        listOfInventories,
        hobMode=True,
        dpsVersion=False,
        rune="HoB",
        minorRune1="AS",
        minorRune2="AD",
        hasTranscendence=False,
        hasCosmic=False,
        currentPercentHP=0.5,
        ultimate_on=True,
        inputDps=InputDps(25, 100, 100),
        sortIndexes=[6],
    )

    # LVL 16 ~ 11000+ gold


if __name__ == "__main__":
    # Expected gold for lvl 1 is 505 gold
    # Expected gold for lvl 2 is 610 gold
    # Expected gold for lvl 3 is 814 gold
    # Expected gold for lvl 4 is 1098 gold
    # Expected gold for lvl 5 is 1446 gold
    # Expected gold for lvl 6 is 1851 gold
    # Expected gold for lvl 7 is 2311 gold
    # Expected gold for lvl 8 is 2830 gold
    # Expected gold for lvl 9 is 3416 gold
    # Expected gold for lvl 10 is 4084 gold
    # Expected gold for lvl 11 is 4856 gold
    # Expected gold for lvl 12 is 5757 gold
    # Expected gold for lvl 13 is 6821 gold
    # Expected gold for lvl 14 is 8085 gold
    # Expected gold for lvl 15 is 9593 gold
    # Expected gold for lvl 16 is 11396 gold
    # Expected gold for lvl 17 is 13548 gold
    # Expected gold for lvl 18 is 16111 gold
    print("A new day dawns for Cait")
    parser = argparse.ArgumentParser(
        formatter_class=RawTextHelpFormatter,
        description="Various optimizations available : ideal stats, ideal items, inventory comparison. Usage example: \npython3 main.py --lvl 9 --hp 0.01 --dps-mode\npython3 main.py --lvl 18 --hp 1.0 --hob-mode",
    )

    parser.add_argument(
        "--lvl",
        type=int,
        help="If present, only this lvl will be considered",
        default=0,
    )
    parser.add_argument(
        "--hp",
        type=float,
        help="Sets the current hp. 0.5 means that the champion is at half health.",
        default=0.5,
    )

    parser.add_argument(
        "--hob-mode",
        dest="hob",
        action="store_true",
        help="If present, uses the HoB exact trading function (choose this or --dps-mode)",
    )
    parser.add_argument(
        "--dps-mode",
        dest="hob",
        action="store_false",
        help="If present, uses the average damage calculation function (choose this or --hob-mode)",
    )
    parser.set_defaults(hob=False)
    args = parser.parse_args()

    # tryndamere_inventory_tests(args)

    # tryndamere_hydra_exporation()

    tryndamere_hob_exporation()
