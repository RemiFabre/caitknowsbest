import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from champ import *
from spell import *
from item import *
from items import *

from utils import (
    getGoldForLvl,
    inventoryCost,
    sort_dict,
    removeInvDuplicates,
    printResults,
    combiSearch2,
    InputDps,
)
from visualization import plot_radar

from tankines_brute import tankiness


def realDamageAndTimeAlive(champ, dps, inputDps, ultimate_on=False):
    """The idea here is that we want to maximize the damage we can do before dying. This is the "real damage".
    The formula is realDmg = timeAlive * dps
    and timeAlive = hp / (incomingDps*damageReduction)
    """
    timeAlive = tankiness(
        champ.hp,
        champ.armor,
        champ.mr,
        inputDps.dps_true,
        inputDps.dps_physical,
        inputDps.dps_magical,
    )
    if champ.name == "Tryndamere" and ultimate_on:
        # Tryndamere can't die for 5 seconds
        timeAlive += 5
    # print(f"Time alive: {timeAlive:.2f}")
    realDamage = timeAlive * dps
    return realDamage, timeAlive


def statsWorthitude(base_champ, sandBag, inputDps, ultimate_on=False):
    """For a given champ and inventory, calculates which stats maximise: turretDps, champDps, realDamage.
    turretDps = the DPS against turrets
    champDps = the pure DPS vs the sandbag
    realDamage = the actual damage dealt before dying"""
    statsValues = [
        AD_COST,
        AS_COST,
        LETHALITY_COST,
        CRIT_COST,
        HASTE_COST,
        HP_COST,
        ARMOR_COST,
        MR_COST,
    ]
    statsNames = ["AD", "AS", "Lethality", "Crit", "Haste", "HP", "Armor", "MR"]
    dps_true = inputDps.dps_true
    dps_physical = inputDps.dps_physical
    dps_magical = inputDps.dps_magical
    # 70 armor is a good estimate of the armor of turrets padt the first one
    turretArmor = 70
    # Buying 500g of each stat, one at a time, and calculating the increase in: turretDps, champDps, realDamage
    step = 500
    maxLethality = 80
    champ = copy.deepcopy(base_champ)
    turretWorthitude = {}
    dpsWorthitude = {}
    realDamageWorthitude = {}

    # Calculating baseline values
    bonusArPen = (
        champ.lethality
    )  # Lethality became armor pen again, was: champ.lethality * (0.6 + 0.4 * champ.lvl / 18.0)
    sandBagArmor = max(0, sandBag.armor * (1 - champ.percentArmorPen) - bonusArPen)
    # Calculating the impact these stats have on the turret DPS
    turretArmorRatio = 100.0 / (100 + max(0, turretArmor - bonusArPen))
    baselineTurretDmg = champ.getAverageDpsTurret(armorRatio=turretArmorRatio)
    # Calculating the impact these stats have on the champion DPS
    armorRatio = 100.0 / (100 + sandBagArmor)
    baselineDmg, _, _ = champ.getAverageDps(armorRatio)
    # Calculating the impact these stats have on the Real Damage
    baselineRealDamage, timeAliveBaseline = realDamageAndTimeAlive(
        champ, baselineDmg, inputDps, ultimate_on=ultimate_on
    )

    print(
        f"Baseline values. turretDmg: {baselineTurretDmg:.2f}, dps: {baselineDmg:.2f}, realDamage: {baselineRealDamage:.2f}"
    )

    for statIndex, statName in enumerate(statsNames):
        bonusAd = step / statsValues[0] if statName == "AD" else 0
        bonusAs = step / (statsValues[1] * 100) if statName == "AS" else 0
        # Can't buy an unlimited amount of lethality.
        bonusLethality = (
            min(maxLethality, step / statsValues[2]) if statName == "Lethality" else 0
        )
        bonusCrit = min(1.0, step / (statsValues[3] * 100)) if statName == "Crit" else 0
        bonusHaste = step / statsValues[4] if statName == "Haste" else 0
        bonusHp = step / statsValues[5] if statName == "HP" else 0
        bonusArmor = step / statsValues[6] if statName == "Armor" else 0
        bonusMr = step / statsValues[7] if statName == "MR" else 0

        fakeItem = Item(
            "fake",
            step,
            step,
            [],
            bonusAd=bonusAd,
            bonusAs=bonusAs,
            bonusOnHit=0,
            bonusCritChance=bonusCrit,
            bonusLethality=bonusLethality,
            bonusHaste=bonusHaste,
            bonusHp=bonusHp,
            bonusArmor=bonusArmor,
            bonusMr=bonusMr,
        )
        # print(f"\nbuying {step}g of {statName}. Equivalent item:\n{fakeItem}")
        # Clearing inventory for next step
        champ = copy.deepcopy(base_champ)
        champ.inventory.append(fakeItem)
        champ.update()

        # ArPen can't reduce armor below 0 (armor reduction can though)
        bonusArPen = champ.lethality * (0.6 + 0.4 * champ.lvl / 18.0)
        sandBagArmor = max(0, sandBag.armor - bonusArPen) * (1 - champ.percentArmorPen)

        # Calculating the impact these stats have on the turret DPS
        # 70 armor is a good estimate of the armor of turrets padt the first one
        turretArmorRatio = 100.0 / (100 + max(0, turretArmor - bonusArPen))

        turretDmg = champ.getAverageDpsTurret(armorRatio=turretArmorRatio)

        # Calculating the impact these stats have on the champion DPS
        armorRatio = 100.0 / (100 + sandBagArmor)
        dmg, spellDmg, autoDmg = champ.getAverageDps(armorRatio)

        # Calculating the impact these stats have on the Real Damage
        realDamage, timeAlive = realDamageAndTimeAlive(
            champ, dmg, inputDps, ultimate_on=ultimate_on
        )

        # Store the results
        turretWorthitude[statName] = -baselineTurretDmg + turretDmg
        dpsWorthitude[statName] = -baselineDmg + dmg
        realDamageWorthitude[statName] = -baselineRealDamage + realDamage
        print(
            f"Impact of {statName}. turretDmg: {turretWorthitude[statName]:.2f}, dps: {dpsWorthitude[statName]:.2f}, realDamage: {realDamageWorthitude[statName]:.2f}"
        )
    inputDpsDict = {
        "True dmg": dps_true,
        "Physical dmg": dps_physical,
        "Magical dmg": dps_magical,
    }
    # Data visualization
    # sns.set_theme()
    fig, axes = plt.subplots(1, 4, figsize=(18, 7), subplot_kw=dict(polar=True))
    fig.subplots_adjust(top=1.0)
    plot_radar(axes[0], turretWorthitude, "Turret DPS")
    plot_radar(axes[1], dpsWorthitude, "Champion DPS")
    plot_radar(axes[2], realDamageWorthitude, "Real Damage before dying")
    plot_radar(axes[3], inputDpsDict, "Incoming damage distribution", color="red")

    fig.suptitle("How valuable are stats?", fontsize=20)
    # plt.subplots_adjust(bottom=0.1)

    ultimate_text = "WITH ultimate" if ultimate_on else "WITHOUT ultimate"
    rune_text = (
        "WITH Lethal Tempo stacked" if champ.rune == "Tempo" else "WITHOUT Lethal Tempo"
    )

    items_text = ""
    for item in champ.inventory[:-1]:
        items_text += f"{item.name} "
    custom_text = f"Level {champ.lvl} {champ.name} {ultimate_text}\nItems {items_text}\n{rune_text}"
    custom_text1 = f"Turret DPS {baselineTurretDmg:.0f}\nChampion DPS {baselineDmg:.0f}\nReal Damage done {baselineRealDamage:.0f}"
    custom_text2 = f"{champ.name} survives for {timeAliveBaseline:.2f} seconds\nOpponent's armor {sandBag.armor:.0f}"
    plt.figtext(0.02, 0.05, custom_text, fontsize=14, color="blue", ha="left")
    plt.figtext(0.4, 0.05, custom_text1, fontsize=14, color="red", ha="left")
    plt.figtext(0.65, 0.05, custom_text2, fontsize=14, color="grey", ha="left")
    plt.tight_layout()
    title_save = (
        "plots/"
        + champ.name
        + "_lvl"
        + str(champ.lvl)
        + "_"
        + str(champ.rune)
        + "_"
        + items_text
        + "_"
        + ultimate_text
        + "_incDmg"
        + str(dps_true)
        + "_"
        + str(dps_physical)
        + "_"
        + str(dps_magical)
    )
    plt.savefig(title_save + ".pdf", bbox_inches="tight")
    plt.show()

    return turretWorthitude, dpsWorthitude, realDamageWorthitude, inputDpsDict


def idealBuyForLvlVsChampCombi(
    champ,
    sandBag,
    listOfSpells,
    fightDuration,
    lvl,
    stepsPerSearch=10,
    dpsVersion=True,
    bruiserMode=False,
    ultimate_on=False,
    inputDps=InputDps(25, 100, 100),
):
    # The (5) stats will be searched and read in the following order
    statsValues = [
        AD_COST,
        AS_COST,
        CRIT_COST,
        LETHALITY_COST,
        HASTE_COST,
    ]
    statsNames = [
        "AD",
        "AS",
        "Crit",
        "Lethality",
        "Haste",
    ]
    if bruiserMode:
        statsValues.append(HP_COST)
        statsValues.append(ARMOR_COST)
        statsNames.append("HP")
        statsNames.append("Armor")

    totalGold = getGoldForLvl(lvl)

    idealAd = 0
    idealAs = 0
    idealCrit = 0
    idealLethality = 0
    idealHaste = 0
    idealDmg = 0
    idealHp = 0
    idealArmor = 0
    idealTimeAlive = 0
    idealRealDmg = 0
    idealIncomingDmg = 0
    idealNbAutos = 0
    maxLethality = 80

    baseArmor = sandBag.armor

    # Searching the entire space
    resultList = []
    # Putting a 0 in a slot deactivates its search from a combinatory (or cpu usage) perspective.
    if not bruiserMode:
        max_steps_per_search = [
            stepsPerSearch,
            stepsPerSearch,
            stepsPerSearch,
            stepsPerSearch,
            stepsPerSearch,
        ]
    else:
        # In bruiser mode we can't brute force all the dimensions. Taking away the lethality dimension
        max_steps_per_search = [
            stepsPerSearch,
            stepsPerSearch,
            stepsPerSearch,
            0,
            stepsPerSearch,
            stepsPerSearch,
            stepsPerSearch,
        ]

    resultList = combiSearch2(
        max_steps_per_search,
        stepsPerSearch,
    )
    # print ("resultList = {}".format(resultList))
    print("nb viable combinations = {}".format(len(resultList)))
    step = totalGold / float(stepsPerSearch)

    # Transforming our number of buys into stats and evaluating their effectivness
    for combi in resultList:
        bonusAd = combi[0] * step / statsValues[0]
        bonusAs = combi[1] * step / (statsValues[1] * 100)
        bonusCrit = min(1.0, combi[2] * step / (statsValues[2] * 100))
        # Can't buy an unlimited amount of lethality.
        bonusLethality = min(maxLethality, combi[3] * step / statsValues[3])
        bonusArPen = bonusLethality  # bonusLethality * (0.6 + 0.4 * champ.lvl / 18.0)
        bonusHaste = combi[4] * step / statsValues[4]
        if bruiserMode:
            bonusHp = combi[5] * step / statsValues[5]
            bonusArmor = combi[6] * step / statsValues[6]
        else:
            bonusHp = 0
            bonusArmor = 0
        # ArPen can't reduce armor below 0 (armor reduction can though)
        sandBagArmor = max(0, baseArmor - bonusArPen) * (1 - champ.percentArmorPen)
        fakeItem = Item(
            "fake",
            totalGold,
            totalGold,
            [],
            bonusAd=bonusAd,
            bonusAs=bonusAs,
            bonusOnHit=0,
            bonusCritChance=bonusCrit,
            bonusHaste=bonusHaste,
            bonusHp=bonusHp,
            bonusArmor=bonusArmor,
        )
        # Clearing inventory for next step
        champ.inventory = []
        champ.lvl = lvl
        champ.inventory.append(fakeItem)
        champ.update()

        # We use at least 1 auto no matter what
        nbAutos = max(1, fightDuration * champ.as_)
        armorRatio = 100.0 / (100 + sandBagArmor)
        if not (dpsVersion):
            dmg, spellDmg, autoDmg = champ.getDmgFromActions(listOfSpells, nbAutos)
            dmg = dmg * armorRatio
            spellDmg = spellDmg * armorRatio
            autoDmg = autoDmg * armorRatio
        else:
            dmg, spellDmg, autoDmg = champ.getAverageDps(armorRatio)
        # print "*dmg = ", dmg, ", spellDmg = ", spellDmg, ", autoDmg = ", autoDmg, "with ad = ", bonusAd, ", as = ", bonusAs, ", crit = ", bonusCrit, ", arPen = ", bonusArPen, ", nbAutos = ", nbAutos, " ieOn = ", ieOn
        if not bruiserMode:
            # Only damage matters
            if dmg > idealDmg:
                idealDmg = dmg
                idealSpellDmg = spellDmg
                idealAutoDmg = autoDmg
                idealAd = bonusAd
                idealAs = bonusAs
                idealCrit = bonusCrit
                idealLethality = bonusLethality
                idealHaste = bonusHaste
                idealNbAutos = nbAutos
        else:
            # Calculating the impact these stats have on the Real Damage
            realDamage, timeAlive = realDamageAndTimeAlive(
                champ, dmg, inputDps, ultimate_on=ultimate_on
            )

            if realDamage > idealRealDmg:
                idealDmg = dmg
                idealSpellDmg = spellDmg
                idealAutoDmg = autoDmg
                idealAd = bonusAd
                idealAs = bonusAs
                idealCrit = bonusCrit
                idealLethality = bonusLethality
                idealHaste = bonusHaste
                idealNbAutos = nbAutos
                idealRealDmg = realDamage
                idealHp = bonusHp
                idealArmor = bonusArmor
                idealTimeAlive = timeAlive

    # This resets the champions stats (that were modified by items), just to print them
    champ.updateStatsForLvl(lvl)
    print(
        "\n***Lvl : "
        + str(lvl)
        + ", gold = "
        + str(totalGold).strip()
        + ", goldStep = "
        + str(step).strip()
        + ", baseAd = "
        + "{0:.1f}".format(champ.ad)
        + ", baseAs = "
        + "{0:.1f}".format(champ.as_)
        + ", Sandbag's armor = "
        + "{0:.1f}".format(baseArmor)
        + ", nbCombinations = "
        + str(len(resultList))
    )
    return [
        idealDmg,
        idealAd,
        idealAs,
        idealCrit,
        idealLethality,
        idealHaste,
        idealSpellDmg,
        idealAutoDmg,
        idealNbAutos,
        totalGold,
        step,
        champ.ad,
        champ.as_,
        baseArmor,
        len(resultList),
        idealHp,
        idealArmor,
        idealTimeAlive,
        idealRealDmg,
    ]


def idealItemsForLvlVsChampCombi(
    combi,
    champ,
    sandBag,
    listOfSpells,
    fightDuration,
    lvl,
    rich=False,
    dpsVersion=False,
    isTiamatFirst=False,
    bootsMandatoryLvl=11,
    slotsAvailable=6,
    hobMode=False,
    ultimate_on=False,
    inputDps=InputDps(25, 100, 100),
):
    """
    This function brute forces every combination of items as long as the money allows it.
    For simplification purposes, you can only buy an item once
    2020 version of this function with tryn in mind.
    """
    if rich:
        totalGold = 30000
    else:
        totalGold = getGoldForLvl(lvl)
    iters = 0
    inventory = []
    baseArmor = sandBag.armor
    results = []

    print(
        "Dps optimization for champion {} lvl {} on {} available inventory slots with {} gold\n".format(
            champ.name, lvl, slotsAvailable, totalGold
        )
    )
    if isTiamatFirst and lvl < 18:
        print("Tiamat is now mandatory")
        slotsAvailable -= 1
    if lvl >= bootsMandatoryLvl:
        print("Boots are now mandatory")
        slotsAvailable -= 1

    # print ("invs = {}".format(combi))
    for invSize in combi:
        for inv in invSize:
            iters = iters + 1
            # Filtering viable builds
            # print ("inv = {}".format(inv))
            # Filtering some items with same unique passive
            if (
                (containsItem(inv, "tiamat") and containsItem(inv, "hydra"))
                or (
                    containsItem(inv, "lordDominik")
                    and containsItem(inv, "lastWhisper")
                )
                or (containsItem(inv, "serylda") and containsItem(inv, "lastWhisper"))
                or (containsItem(inv, "serylda") and containsItem(inv, "lordDominik"))
            ):
                continue
            if (
                isTiamatFirst
                and lvl < 18
                and totalGold >= ITEMS["tiamat"].totalCost
                and inv[0].name != "None"
            ):
                if containsItem(inv, "tiamat") == False:
                    continue

            if lvl >= bootsMandatoryLvl:
                if (
                    containsItem(inv, "ionanBoots") == False
                    and containsItem(inv, "berserkerBoots") == False
                    and inv[0].name != "None"
                ):
                    continue
            cost = inventoryCost(inv)
            if cost > totalGold:
                # Too expensive
                continue
            if not (rich) and (totalGold - cost) > 1300:
                # Not expensive enough
                continue

            # We have a viable build, let's measure its performance
            inventory = []
            inventory.extend(inv)
            champ.inventory = inventory
            champ.lvl = lvl
            champ.update()
            """
            strInventory = ""
            for i in inventory:
                strInventory = strInventory + " " +  str(i)
            print("Trying inventory :" + strInventory)
            """
            bonusArPen = champ.lethality * (0.6 + 0.4 * champ.lvl / 18.0)
            # ArPen can't reduce armor below 0 (armor reduction can though)
            sandBagArmor = max(0, baseArmor - bonusArPen) * (1 - champ.percentArmorPen)
            armorRatio = 100.0 / (100 + sandBagArmor)
            # We use at least 1 auto no matter what
            nbAutos = max(1, fightDuration * champ.as_)
            turretArmor = 70
            if not (hobMode):
                if not (dpsVersion):
                    print("Old mode, deprecate?")
                    dmg, spellDmg, autoDmg = champ.getDmgFromActions(
                        listOfSpells, nbAutos
                    )
                    dmg = dmg * armorRatio
                    spellDmg = spellDmg * armorRatio
                    autoDmg = autoDmg * armorRatio
                else:
                    dmg, spellDmg, autoDmg = champ.getAverageDps(armorRatio)

                turretDps = champ.getAverageDpsTurret(
                    100.0
                    / (100 + max(0, turretArmor * champ.percentArmorValid - bonusArPen))
                )

                realDamage, timeAlive = realDamageAndTimeAlive(
                    champ, dmg, inputDps, ultimate_on=ultimate_on
                )
                # Just for easier reading, small cpu loss
                header = [
                    "DPS",
                    "Cost",
                    "Build",
                    "SpellSmg",
                    "AutoDmg",
                    "Turret DPS",
                    "RealDmg",
                    "TimeAlive",
                ]
                results.append(
                    [
                        dmg,
                        cost,
                        inventory,
                        spellDmg,
                        autoDmg,
                        turretDps,
                        realDamage,
                        timeAlive,
                    ]
                )
            else:
                # Using the HoB trading pattern calculation
                _, _, dmg, spellDmg, autoDmg, expectednbE = champ.hailTradingPattern(
                    armorRatio=armorRatio, opponentHp=None, verbose=False
                )
                header = ["DPS", "Cost", "Build", "SpellSmg", "AutoDmg", "expectednbE"]
                results.append([dmg, cost, inventory, spellDmg, autoDmg, expectednbE])

    return results, header


def statsWorthitudeTest():
    inventories = []
    # inventories.append([ITEMS["hydra"], ITEMS["berserkerBoots"]])
    # inventories.append([ITEMS["phantomDancer"], ITEMS["berserkerBoots"], TEMP["500gOfAD"], TEMP["300gOfAD"]])
    # inventories.append([ITEMS["hydra"], ITEMS["hullbreaker"]])
    # inventories.append([ITEMS["hydra"], ITEMS["hullbreaker"], ITEMS["zeal"], ITEMS["cloak"]])
    inventories.append(
        [
            ITEMS["berserkerBoots"],
            ITEMS["hydra"],
            ITEMS["hullbreaker"],
            ITEMS["phantomDancer"],
            ITEMS["galeForce"],
            ITEMS["shojin"],
        ]
    )
    for inventory in inventories:
        lvl = 16
        champ = Tryndamere(lvl=lvl, inventory=inventory)
        champ.currentPercentHP = 0.5
        champ.rune = "Tempo"
        champ.hasTranscendence = False
        champ.hasCosmic = False
        champ.minorRune1 = "AS"
        champ.minorRune2 = "AD"
        champ.update()

        sandBag = Tryndamere(lvl=lvl, inventory=[])
        sandBag.update()

        # In the general case, the actual values of the InputDps don't matter when prioritising stats, only the ratio between them does.
        # This is not true when non linearities are involved around tankiness (like with Tryndamere's R)
        statsWorthitude(champ, sandBag, InputDps(25, 100, 100), ultimate_on=False)

        # Creates 12 plots for Tryndamere
        # for inputDps in [InputDps(25, 100, 100), InputDps(5, 210, 10), InputDps(5, 10, 210)]:
        #     # Balanced dmg profile then almost only physical dmg then almost only magical dmg
        #     for ultimate_on in [True, False]:
        #         for rune in ["Tempo", "None"]:
        #             champ.rune = rune
        #             champ.update()
        #             statsWorthitude(champ, sandBag, inputDps, ultimate_on=ultimate_on)

    # Iterate on builds
    # Post?


if __name__ == "__main__":
    statsWorthitudeTest()
